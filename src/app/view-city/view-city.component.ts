import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { City } from '../model/city';
import { State } from '../model/state';
import { AddCityService } from '../service/add-city.service';
import { AddStateService } from '../service/add-state.service';

@Component({
  selector: 'app-view-city',
  templateUrl: './view-city.component.html',
  styleUrls: ['./view-city.component.css']
})
export class ViewCityComponent implements OnInit {
  title = 'appBootstrap';
  public city!: City;
  closeResult: string = '';
  constructor(private stateService:AddStateService,private cityService:AddCityService,private modalService: NgbModal ) { }
  cities!: City[];
  states!: State[];
  ngOnInit(): void {
    this.cityService.getCities().subscribe(
      {
        next: (response) => {
          this.cities = response;
        }
      }
    )
    // this.stateService.getStates().subscribe(
    //   {
    //     next: (response) => {
    //       this.states = response;
    //     }
    //   }
    // )
  }
  updateCityStatus(status:string){ 
    this.cityService.updateCityStatus(status, this.city.cityId).subscribe(
      {
        next: (response) => {
          alert("Status update Seccessfully");
          window.location.reload();
        },
        error: (err) => {
          alert("Update Failed");
        }
      }
    )
  }

  //bootstrap model open
  open(content: any, city: any) {
    this.city = city;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
