import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInsurenceSchemeComponent } from './add-insurence-scheme.component';

describe('AddInsurenceSchemeComponent', () => {
  let component: AddInsurenceSchemeComponent;
  let fixture: ComponentFixture<AddInsurenceSchemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInsurenceSchemeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddInsurenceSchemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
