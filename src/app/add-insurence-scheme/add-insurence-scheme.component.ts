import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { InsuranceScheme } from '../model/insuranceScheme';
import { IInsuranceType } from '../model/InsuranceType';
import { InsuranceSchemeServiceService } from '../service/insurance-scheme-service.service';
import { InsuranceTypeServiceService } from '../service/insurance-type-service.service';

@Component({
  selector: 'app-add-insurence-scheme',
  templateUrl: './add-insurence-scheme.component.html',
  styleUrls: ['./add-insurence-scheme.component.css']
})
export class AddInsurenceSchemeComponent implements OnInit {
  addInsuranceSchemeForm = this.formBuilder.group({
    id:new FormControl(),
    insuranceScheme: new FormControl(),
    commissionForNewRegistration: new FormControl(),
    commissionForInstallmentPayment: new FormControl(),
    note: new FormControl(),
    status:new FormControl()
  });
  insuranceTypes!: IInsuranceType[];
  insuranceSchemes!: InsuranceScheme[];
  insuranceTypeId!: number;
  constructor(private formBuilder:FormBuilder,private insuranceTypeService:InsuranceTypeServiceService,private insuranceSchemeService:InsuranceSchemeServiceService) { }

  ngOnInit(): void {
    this.insuranceTypeService.getActiveInsuranceTypes().subscribe(
      {
        next: (response) => {
          this.insuranceTypes = response;
        }
      }
    )
  }
  setTypeId(id:number) { 
    this.insuranceTypeId = id;
  }
  addInsuranceScheme() { 
    this.insuranceTypeId=this.addInsuranceSchemeForm.value.id;
    this.insuranceSchemeService.addInsurancecheme(this.addInsuranceSchemeForm.value,this.insuranceTypeId).subscribe(
      {
        next: (response) => {
          alert("Adding of Insurance Scheme is Successfully done.");
        },
        error: (err) => {
          console.log(this.addInsuranceSchemeForm.value);
          alert("Registration UnSuccessFull !!");
        }
      }
    )
  }
  
}
