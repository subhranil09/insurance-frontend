import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-add-agent',
  templateUrl: './add-agent.component.html',
  styleUrls: ['./add-agent.component.css'],
})
export class AddAgentComponent implements OnInit {
  // Form = this.formBuilder.group({
  //   agentName: new FormControl(),
  //   agentCode:new FormControl(),
  //   emailId: new FormControl(),
  //   password: new FormControl(),
  //   qualification: new FormControl(),
  //   agentAddress:new FormControl(),
  //   status: new FormControl(),

  // });
  Form = this.formBuilder.group({
    agentName: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z]+$'),
    ]),
    agentCode: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Z0-9]+$'),
    ]),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    qualification: new FormControl(),
    agentAddress: new FormControl(),
    status: new FormControl(),
  });
  constructor(
    private formBuilder: FormBuilder,
    private service: AgentService
  ) {}

  ngOnInit(): void {}
  addAgent(cpassword: string) {
    if (cpassword !== this.Form.value.password) {
      alert("Confirm password dosn't match please check again");
    } else {
      this.service.addAgent(this.Form.value).subscribe({
        next: (response) => {
          alert('Adding of Agent is Successfully done.');
        },
        error: (err) => {
          alert('Registration UnSuccessFull !!');
        },
      });
    }
  }
}
