import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-agent-nav-bar',
  templateUrl: './agent-nav-bar.component.html',
  styleUrls: ['./agent-nav-bar.component.css']
})
export class AgentNavBarComponent implements OnInit {

  constructor(private router :Router) { }

  ngOnInit(): void {
  }
  logout() { 
    localStorage.removeItem("username");
    localStorage.removeItem("userId");
    localStorage.removeItem("role");
    this.router.navigate([""]);

  }
}
