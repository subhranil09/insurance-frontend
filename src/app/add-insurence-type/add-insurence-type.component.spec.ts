import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInsurenceTypeComponent } from './add-insurence-type.component';

describe('AddInsurenceTypeComponent', () => {
  let component: AddInsurenceTypeComponent;
  let fixture: ComponentFixture<AddInsurenceTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInsurenceTypeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddInsurenceTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
