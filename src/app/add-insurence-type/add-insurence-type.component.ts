import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormControl } from '@angular/forms';
import { IInsuranceType } from '../model/InsuranceType';
import { AddInsuranceTypeService } from '../service/add-insurance-type.service';

@Component({
  selector: 'app-add-insurence-type',
  templateUrl: './add-insurence-type.component.html',
  styleUrls: ['./add-insurence-type.component.css']
})
export class AddInsurenceTypeComponent implements OnInit {

  addInsuranceType = this.formBuilder.group({
    insuranceType: new FormControl(),
    status: new FormControl()
  });
  public insuranceType!: IInsuranceType;
  file!: File ;
  constructor(private formBuilder:FormBuilder,private service:AddInsuranceTypeService) { }

  ngOnInit(): void {
  }
  onFilechange(event: any) {
    console.log(event.target.files[0])
    this.file = event.target.files[0]
  }
  createInsuranceType() {
    console.log(this.addInsuranceType.value);
    this.service.postInsuranceType(this.addInsuranceType.value).subscribe(
      {
        next: (response) => {
          this.insuranceType = response;
          console.log(this.insuranceType.insuranceTypeId);
          this.service.putInsuranceTypeImage(this.file,this.insuranceType.insuranceTypeId).subscribe(
            {
              next: (response) => { 
                
              },
              error: (err) => {
                alert("Image upload failed");
              }
            }
          )
          alert("Adding of Insurance Type is Successfully done.");
        },
        error: (err) => {
          alert("Adding of Insurance Type is faild! try again");
        }
      }
    )
    
  }
}


