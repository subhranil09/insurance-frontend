import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { InsuranceScheme } from '../model/insuranceScheme';
import { IInsuranceType } from '../model/InsuranceType';
import { InsurancePlanServiceService } from '../service/insurance-plan-service.service';
import { InsuranceSchemeServiceService } from '../service/insurance-scheme-service.service';
import { InsuranceTypeServiceService } from '../service/insurance-type-service.service';

@Component({
  selector: 'app-add-insurence-plan',
  templateUrl: './add-insurence-plan.component.html',
  styleUrls: ['./add-insurence-plan.component.css']
})
export class AddInsurencePlanComponent implements OnInit {
  addInsurancePlanForm = this.formBuilder.group({
    schemeid:new FormControl(),
    insurancePlan: new FormControl(),
    maximumPolicyTerm: new FormControl(),
    minumumPolicyTerm: new FormControl(),
    minimumAge: new FormControl(),
    maximumAge: new FormControl(),
    minimumInvestmentAmount: new FormControl(),
    maximumInvestmentAmount: new FormControl(),
    profitRatio: new FormControl(),
    status:new FormControl()
  });

  insuranceTypes!: IInsuranceType[];
  insuranceSchemes!: InsuranceScheme[];
  insuranceTypeId!: number;
  insuranceSchemeId!: number;
  constructor(private formBuilder:FormBuilder,private insuranceTypeService:InsuranceTypeServiceService,private insurancePlanService:InsurancePlanServiceService,private insuranceSchemeService:InsuranceSchemeServiceService) { }

  ngOnInit(): void {
    this.insuranceTypeService.getActiveInsuranceTypes().subscribe(
      {
        next: (response) => {
          this.insuranceTypes = response;
        }
      }
    )
  }
  updateTypeId() { 
    this.insuranceSchemeService.getActiveInsuranceSchemeByTypeId(this.insuranceTypeId).subscribe(
      {
        next: (response) => {
          this.insuranceSchemes = response;
        }
      }
    )
  }
  addInsurancePlan() { 
    this.insuranceSchemeId = this.addInsurancePlanForm.value.schemeid;
    console.log(this.addInsurancePlanForm.value.schemeid);
    
    this.insurancePlanService.addInsurancePlan(this.addInsurancePlanForm.value,this.insuranceSchemeId).subscribe(
      {
        next: (response) => {
          alert("Adding of Insurance plan is Successfully done.");
        },
        error: (err) => {
          alert("Adding of Insurance plan is faild !!");
        }
      }
    )
  }
}
