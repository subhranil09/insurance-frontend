import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInsurencePlanComponent } from './add-insurence-plan.component';

describe('AddInsurencePlanComponent', () => {
  let component: AddInsurencePlanComponent;
  let fixture: ComponentFixture<AddInsurencePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddInsurencePlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddInsurencePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
