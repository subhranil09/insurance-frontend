import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainHeaderComponent } from './main-header/main-header.component';
import { AppNavbarComponent } from './app-navbar/app-navbar.component';
import { AppBodyComponent } from './app-body/app-body.component';
import { AppFooterComponent } from './app-footer/app-footer.component';
import { CustomerLoginComponent } from './customer-login/customer-login.component';
import { RegistrationComponent } from './registration/registration.component';
import { AgentLoginComponent } from './agent-login/agent-login.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { EmployeeLoginComponent } from './employee-login/employee-login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component'
import { AddAgentComponent } from './add-agent/add-agent.component';
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { ViewAgentComponent } from './view-agent/view-agent.component';
import { ViewCommissionComponent } from './view-commission/view-commission.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { ViewCommissionWithdrawlComponent } from './view-commission-withdrawl/view-commission-withdrawl.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminViewCustomersComponent } from './admin-view-customers/admin-view-customers.component';
import { AdminViewInsuranceAccountDetailComponent } from './admin-view-insurance-account-detail/admin-view-insurance-account-detail.component';
import { AdminViewPaymentsComponent } from './admin-view-payments/admin-view-payments.component';
import { AdminViewPolicyclaimsComponent } from './admin-view-policyclaims/admin-view-policyclaims.component';
import { AdminViewFeedBackComponent } from './admin-view-feed-back/admin-view-feed-back.component';
import { AddInsurenceTypeComponent } from './add-insurence-type/add-insurence-type.component';
import { ViewInsurenceTypeComponent } from './view-insurence-type/view-insurence-type.component';
import { AddInsurenceSchemeComponent } from './add-insurence-scheme/add-insurence-scheme.component';
import { ViewInsurenceSchemeComponent } from './view-insurence-scheme/view-insurence-scheme.component';
import { AddInsurencePlanComponent } from './add-insurence-plan/add-insurence-plan.component';
import { ViewInsurencePlanComponent } from './view-insurence-plan/view-insurence-plan.component';
import { TaxPanelComponent } from './tax-panel/tax-panel.component';
import { InsuranceSettingComponent } from './insurance-setting/insurance-setting.component';
import { AddStateComponent } from './add-state/add-state.component';
import { ViewStateComponent } from './view-state/view-state.component';
import { AddCityComponent } from './add-city/add-city.component';
import { ViewCityComponent } from './view-city/view-city.component';
import { AdminProfileUpdateComponent } from './admin-profile-update/admin-profile-update.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { UpdateAgentComponentComponent } from './update-agent-component/update-agent-component.component';
import { CustomerInsuranceAccountComponent } from './customer-insurance-account/customer-insurance-account.component';
import { EmployeeHomePageComponent } from './employee-home-page/employee-home-page.component';
import { EmployeeDashBoardComponent } from './employee-dash-board/employee-dash-board.component';
import { EmployeeNavBarComponent } from './employee-nav-bar/employee-nav-bar.component';
import { ViewDocumentComponent } from './view-document/view-document.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddCustomerServiceService } from './service/add-customer-service.service';
import { HttpClientModule } from '@angular/common/http';
import { AddEmpServiceService } from './service/add-emp-service.service';
import { UpdateStateStatusComponent } from './update-state-status/update-state-status.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CustomerHomePageComponent } from './customer-home-page/customer-home-page.component';
import { CustomerNavBarComponent } from './customer-nav-bar/customer-nav-bar.component';
import { CustomerDashBoardComponent } from './customer-dash-board/customer-dash-board.component';
import { CustomerProfileUpdateComponent } from './customer-profile-update/customer-profile-update.component';
import { CustomerDocumetsComponent } from './customer-documets/customer-documets.component';
import { CustomerChangePasswordComponent } from './customer-change-password/customer-change-password.component';
import { CustomerViewInsuranceAccountComponent } from './customer-view-insurance-account/customer-view-insurance-account.component';
import { CustomerQueryComponent } from './customer-query/customer-query.component';
import { CustomerViewFeedbackComponent } from './customer-view-feedback/customer-view-feedback.component';
import { AgentHomePageComponent } from './agent-home-page/agent-home-page.component';
import { AgentNavBarComponent } from './agent-nav-bar/agent-nav-bar.component';
import { AgentDashBoardComponent } from './agent-dash-board/agent-dash-board.component';
import { AgentupdateComponent } from './agentupdate/agentupdate.component';
import { AgentchangePasswordComponent } from './agentchange-password/agentchange-password.component';
import { AgentMarketingComponent } from './agent-marketing/agent-marketing.component';
import { AgentViewCustomersComponent } from './agent-view-customers/agent-view-customers.component';
import { AgentViewInsuranceAccountDetailComponent } from './agent-view-insurance-account-detail/agent-view-insurance-account-detail.component';
import { AgentViewPaymentsComponent } from './agent-view-payments/agent-view-payments.component';
import { AgentViewPolicyclaimsComponent } from './agent-view-policyclaims/agent-view-policyclaims.component';
import { AgentViewCommissionsComponent } from './agent-view-commissions/agent-view-commissions.component';
import { AgentViewCommissionWithdrawalsComponent } from './agent-view-commission-withdrawals/agent-view-commission-withdrawals.component';
import { AgentWithdrawComponent } from './agent-withdraw/agent-withdraw.component';
import { CustomerViewPlanComponent } from './customer-view-plan/customer-view-plan.component';
import { GetPlanDetalsService } from './service/get-plan-detals.service';
import { CustomerMakePaymentComponent } from './customer-make-payment/customer-make-payment.component';
@NgModule({
  declarations: [
    AppComponent,
    MainHeaderComponent,
    AppNavbarComponent,
    AppBodyComponent,
    AppFooterComponent,
    CustomerLoginComponent,
    RegistrationComponent,
    AgentLoginComponent,
    AdminLoginComponent,
    EmployeeLoginComponent,
    PageNotFoundComponent,
    AdminNavBarComponent,
    AddAgentComponent,
    AdminHomepageComponent,
    ViewAgentComponent,
    ViewCommissionComponent,
    AddEmployeeComponent,
    ViewCommissionWithdrawlComponent,
    AdminDashboardComponent,
    AdminViewCustomersComponent,
    AdminViewInsuranceAccountDetailComponent,
    AdminViewPaymentsComponent,
    AdminViewPolicyclaimsComponent,
    AdminViewFeedBackComponent,
    AddInsurenceTypeComponent,
    ViewInsurenceTypeComponent,
    AddInsurenceSchemeComponent,
    ViewInsurenceSchemeComponent,
    AddInsurencePlanComponent,
    ViewInsurencePlanComponent,
    TaxPanelComponent,
    InsuranceSettingComponent,
    AddStateComponent,
    ViewStateComponent,
    AddCityComponent,
    ViewCityComponent,
    AdminProfileUpdateComponent,
    AdminChangePasswordComponent,
    ViewEmployeeComponent,
    UpdateAgentComponentComponent,
    CustomerInsuranceAccountComponent,
    EmployeeHomePageComponent,
    EmployeeDashBoardComponent,
    EmployeeNavBarComponent,
    ViewDocumentComponent,
    UpdateStateStatusComponent,
    CustomerHomePageComponent,
    CustomerNavBarComponent,
    CustomerDashBoardComponent,
    CustomerProfileUpdateComponent,
    CustomerDocumetsComponent,
    CustomerChangePasswordComponent,
    CustomerViewInsuranceAccountComponent,
    CustomerQueryComponent,
    CustomerViewFeedbackComponent,
    AgentHomePageComponent,
    AgentNavBarComponent,
    AgentDashBoardComponent,
    AgentupdateComponent,
    AgentchangePasswordComponent,
    AgentMarketingComponent,
    AgentViewCustomersComponent,
    AgentViewInsuranceAccountDetailComponent,
    AgentViewPaymentsComponent,
    AgentViewPolicyclaimsComponent,
    AgentViewCommissionsComponent,
    AgentViewCommissionWithdrawalsComponent,
    AgentWithdrawComponent,
    CustomerViewPlanComponent,
    CustomerMakePaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [AddCustomerServiceService,AddEmpServiceService,GetPlanDetalsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
