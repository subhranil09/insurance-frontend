import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Credentials } from '../model/credential';
import { CustomerServiceService } from '../service/customer-service.service';

@Component({
  selector: 'app-customer-login',
  templateUrl: './customer-login.component.html',
  styleUrls: ['./customer-login.component.css']
})
export class CustomerLoginComponent implements OnInit {
  value!:string;
  credentialFrom = this.formBuilder.group({
    email:new FormControl(),
    password: new FormControl(),
    
  });
  constructor(private formBuilder:FormBuilder,private service:CustomerServiceService,private router:Router) { }
  // user!: LoginResponse;
  ngOnInit(): void {
  }
  login() {
    
    console.log(this.credentialFrom.value);
    
    this.service.auth(this.credentialFrom.value).subscribe({
      next:(response)=>{
        let loginResponse:Credentials = response;
        localStorage.setItem("username", loginResponse.name);
        localStorage.setItem("ROLE", loginResponse.role);
        localStorage.setItem("userId", String(loginResponse.id));
        console.log(response);
          this.router.navigate(["/customer"])
        
      },
      error: (err) => {
        this.value="Invalid Credentials"
        
      }
    })
}

}
