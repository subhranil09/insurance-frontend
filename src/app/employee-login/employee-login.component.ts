import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Credentials } from '../model/credential';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-employee-login',
  templateUrl: './employee-login.component.html',
  styleUrls: ['./employee-login.component.css']
})
export class EmployeeLoginComponent implements OnInit {
  value!:string;
  credentialFrom = this.formBuilder.group({
    email:new FormControl(),
    password: new FormControl(),
    
  });
  constructor(private formBuilder:FormBuilder,private service:EmployeeServiceService,private router:Router) { }
  // user!: LoginResponse;
  ngOnInit(): void {
  }
  login() {
    
    console.log(this.credentialFrom.value);
    
    this.service.auth(this.credentialFrom.value).subscribe({
      next:(response)=>{
        let loginResponse:Credentials = response;
        
        if (loginResponse.role === "ROLE_Employee") {
          localStorage.setItem("username", loginResponse.name);
          localStorage.setItem("ROLE", loginResponse.role);
          localStorage.setItem("userId", String(loginResponse.id));
          this.router.navigate(["/employee"])
        }
        
      },
      error: (err) => {
        this.value="Invalid Credentials"
        
      }
    })
}

}
