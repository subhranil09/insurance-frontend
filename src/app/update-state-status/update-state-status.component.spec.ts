import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateStateStatusComponent } from './update-state-status.component';

describe('UpdateStateStatusComponent', () => {
  let component: UpdateStateStatusComponent;
  let fixture: ComponentFixture<UpdateStateStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateStateStatusComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UpdateStateStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
