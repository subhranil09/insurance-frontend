import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { State } from '../model/state';
import { AddStateService } from '../service/add-state.service';

@Component({
  selector: 'app-update-state-status',
  templateUrl: './update-state-status.component.html',
  styleUrls: ['./update-state-status.component.css']
})
export class UpdateStateStatusComponent implements OnInit {

  UpdateStateForm = this.formBuilder.group({
    state:new FormControl(),
    status: new FormControl(),
  });
  private stateId!: any;
  public state!:State;
  private _route: any;
  constructor(private routh:ActivatedRoute, private formBuilder:FormBuilder,private service:AddStateService) { }

  ngOnInit(): void {
    this._route.paramMap.subscribe((params: ParamMap) =>  {
      this.stateId = params.get('stateId');
    });
    console.log(this.stateId);

    this.service.getStateById(this.stateId).subscribe(
      {
        next: (response) => {
          this.state = response;
        }
      }
    )
  }
  updateStateStatus(status:string){ 
    this.service.addState(this.UpdateStateForm.value).subscribe(
      {
        next: (response) => {
          alert("Status update Seccessfully ");
        },
        error: (err) => {
          alert("Cannot Add The State");
        }
      }
    )
  }

}
