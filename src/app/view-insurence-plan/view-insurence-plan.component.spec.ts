import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInsurencePlanComponent } from './view-insurence-plan.component';

describe('ViewInsurencePlanComponent', () => {
  let component: ViewInsurencePlanComponent;
  let fixture: ComponentFixture<ViewInsurencePlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInsurencePlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewInsurencePlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
