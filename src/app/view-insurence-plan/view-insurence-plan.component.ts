import { Component, OnInit } from '@angular/core';
import { InsurancePlan } from '../model/insurancePlan';

import { InsurancePlanServiceService } from '../service/insurance-plan-service.service';
import { InsuranceScheme } from '../model/insuranceScheme'
@Component({
  selector: 'app-view-insurence-plan',
  templateUrl: './view-insurence-plan.component.html',
  styleUrls: ['./view-insurence-plan.component.css']
})
export class ViewInsurencePlanComponent implements OnInit {
  insurancePlans!: InsurancePlan[];
  constructor(private insurancePlanService: InsurancePlanServiceService) { }

  ngOnInit(): void {
    this.insurancePlanService.getInsurancePlans().subscribe(
      {
        next: (response) => {
          this.insurancePlans = response;
          
        }
      }
    )
  
  }

}
