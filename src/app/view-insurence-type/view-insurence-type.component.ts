import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { IInsuranceType } from '../model/InsuranceType';
import { AddStateService } from '../service/add-state.service';
import { InsuranceTypeServiceService } from '../service/insurance-type-service.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-insurence-type',
  templateUrl: './view-insurence-type.component.html',
  styleUrls: ['./view-insurence-type.component.css']
})
export class ViewInsurenceTypeComponent implements OnInit {
  closeResult: string = '';
  insuranceTypes!: IInsuranceType[];
  constructor(private insuranceTypeService:InsuranceTypeServiceService,private modalService: NgbModal) { }
  insuranceType!: IInsuranceType;
  ngOnInit(): void {
    this.insuranceTypeService.getInsuranceTypes().subscribe(
      {
        next: (response) => {
          this.insuranceTypes = response;
        }
      }
    )
  }
  updateCityStatus(status: string) { 
    this.insuranceType.status = status;
    this.insuranceTypeService.updateInsuranceTypes( this.insuranceType).subscribe(
      {
        next: (response) => {
          alert("Status update Seccessfully");
          window.location.reload();
        },
        error: (err) => {
          alert("Update Failed");
        }
      }
    )
  }
  open(content: any, insuranceType: any) {
    this.insuranceType = insuranceType;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
