import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInsurenceTypeComponent } from './view-insurence-type.component';

describe('ViewInsurenceTypeComponent', () => {
  let component: ViewInsurenceTypeComponent;
  let fixture: ComponentFixture<ViewInsurenceTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInsurenceTypeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewInsurenceTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
