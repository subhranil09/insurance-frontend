import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-agent-dash-board',
  templateUrl: './agent-dash-board.component.html',
  styleUrls: ['./agent-dash-board.component.css'],
})
export class AgentDashBoardComponent implements OnInit {
  user!: any;
  constructor() {}

  ngOnInit(): void {
    this.user = localStorage.getItem('username');
  }
}
