import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewPolicyclaimsComponent } from './admin-view-policyclaims.component';

describe('AdminViewPolicyclaimsComponent', () => {
  let component: AdminViewPolicyclaimsComponent;
  let fixture: ComponentFixture<AdminViewPolicyclaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewPolicyclaimsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminViewPolicyclaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
