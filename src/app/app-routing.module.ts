import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddAgentComponent } from './add-agent/add-agent.component';
import { AddCityComponent } from './add-city/add-city.component';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { AddInsurencePlanComponent } from './add-insurence-plan/add-insurence-plan.component';
import { AddInsurenceSchemeComponent } from './add-insurence-scheme/add-insurence-scheme.component';
import { AddInsurenceTypeComponent } from './add-insurence-type/add-insurence-type.component';
import { AddStateComponent } from './add-state/add-state.component';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminHomepageComponent } from './admin-homepage/admin-homepage.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { AdminProfileUpdateComponent } from './admin-profile-update/admin-profile-update.component';
import { AdminViewCustomersComponent } from './admin-view-customers/admin-view-customers.component';
import { AdminViewFeedBackComponent } from './admin-view-feed-back/admin-view-feed-back.component';
import { AdminViewInsuranceAccountDetailComponent } from './admin-view-insurance-account-detail/admin-view-insurance-account-detail.component';
import { AdminViewPaymentsComponent } from './admin-view-payments/admin-view-payments.component';
import { AdminViewPolicyclaimsComponent } from './admin-view-policyclaims/admin-view-policyclaims.component';
import { AgentDashBoardComponent } from './agent-dash-board/agent-dash-board.component';
import { AgentHomePageComponent } from './agent-home-page/agent-home-page.component';
import { AgentLoginComponent } from './agent-login/agent-login.component';
import { AgentMarketingComponent } from './agent-marketing/agent-marketing.component';
import { AgentViewCommissionWithdrawalsComponent } from './agent-view-commission-withdrawals/agent-view-commission-withdrawals.component';
import { AgentViewCommissionsComponent } from './agent-view-commissions/agent-view-commissions.component';
import { AgentViewCustomersComponent } from './agent-view-customers/agent-view-customers.component';
import { AgentViewInsuranceAccountDetailComponent } from './agent-view-insurance-account-detail/agent-view-insurance-account-detail.component';
import { AgentViewPaymentsComponent } from './agent-view-payments/agent-view-payments.component';
import { AgentViewPolicyclaimsComponent } from './agent-view-policyclaims/agent-view-policyclaims.component';
import { AgentWithdrawComponent } from './agent-withdraw/agent-withdraw.component';
import { AgentchangePasswordComponent } from './agentchange-password/agentchange-password.component';
import { AgentupdateComponent } from './agentupdate/agentupdate.component';
import { AppBodyComponent } from './app-body/app-body.component';
import { CustomerChangePasswordComponent } from './customer-change-password/customer-change-password.component';
import { CustomerDashBoardComponent } from './customer-dash-board/customer-dash-board.component';
import { CustomerDocumetsComponent } from './customer-documets/customer-documets.component';
import { CustomerHomePageComponent } from './customer-home-page/customer-home-page.component';
import { CustomerInsuranceAccountComponent } from './customer-insurance-account/customer-insurance-account.component';
import { CustomerLoginComponent } from './customer-login/customer-login.component';
import { CustomerMakePaymentComponent } from './customer-make-payment/customer-make-payment.component';
import { CustomerProfileUpdateComponent } from './customer-profile-update/customer-profile-update.component';
import { CustomerQueryComponent } from './customer-query/customer-query.component';
import { CustomerViewFeedbackComponent } from './customer-view-feedback/customer-view-feedback.component';
import { CustomerViewInsuranceAccountComponent } from './customer-view-insurance-account/customer-view-insurance-account.component';
import { CustomerViewPlanComponent } from './customer-view-plan/customer-view-plan.component';
import { EmployeeDashBoardComponent } from './employee-dash-board/employee-dash-board.component';
import { EmployeeHomePageComponent } from './employee-home-page/employee-home-page.component';
import { EmployeeLoginComponent } from './employee-login/employee-login.component';
import { InsuranceSettingComponent } from './insurance-setting/insurance-setting.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { RegistrationComponent } from './registration/registration.component';
import { TaxPanelComponent } from './tax-panel/tax-panel.component';
import { UpdateAgentComponentComponent } from './update-agent-component/update-agent-component.component';
import { UpdateStateStatusComponent } from './update-state-status/update-state-status.component';
import { ViewAgentComponent } from './view-agent/view-agent.component';
import { ViewCityComponent } from './view-city/view-city.component';
import { ViewCommissionWithdrawlComponent } from './view-commission-withdrawl/view-commission-withdrawl.component';
import { ViewCommissionComponent } from './view-commission/view-commission.component';
import { ViewEmployeeComponent } from './view-employee/view-employee.component';
import { ViewInsurencePlanComponent } from './view-insurence-plan/view-insurence-plan.component';
import { ViewInsurenceSchemeComponent } from './view-insurence-scheme/view-insurence-scheme.component';
import { ViewInsurenceTypeComponent } from './view-insurence-type/view-insurence-type.component';
import { ViewStateComponent } from './view-state/view-state.component';

const routes: Routes = [
  {
    path: '',
    component: AppBodyComponent,
    pathMatch: 'full',
  },
  {
    path: 'customerLogin',
    component: CustomerLoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'agentLogin',
    component: AgentLoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'employeeLogin',
    component: EmployeeLoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'adminLogin',
    component: AdminLoginComponent,
    pathMatch: 'full',
  },
  {
    path: 'customerRegistration',
    component: RegistrationComponent,
    pathMatch: 'full',
  },
  {
    path: 'admin',
    component: AdminHomepageComponent,
    children: [
      {
        path: '',
        component: AdminDashboardComponent,
      },
      {
        path: 'addAgent',
        component: AddAgentComponent,
      },
      {
        path: 'viewAgent',
        component: ViewAgentComponent,
        children: [
          {
            path: 'updateAgent',
            redirectTo: '/admin/updateAgent',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewCommission',
        component: ViewCommissionComponent,
      },
      {
        path: 'viewCommissionWithrawl',
        component: ViewCommissionWithdrawlComponent,
      },
      {
        path: 'viewFeedback',
        component: AdminViewFeedBackComponent,
      },
      {
        path: 'viewCustomers',
        component: AdminViewCustomersComponent,
        children: [
          {
            path: 'viewDocuments',
            redirectTo: '/admin/viewInsuranceDetails',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewInsuranceAccountDetailgent',
        component: AdminViewInsuranceAccountDetailComponent,
        children: [
          {
            path: 'viewInsuranceDetails',
            redirectTo: '/admin/viewInsuranceDetails',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewPayments',
        component: AdminViewPaymentsComponent,
      },
      {
        path: 'viewPolicyclaims',
        component: AdminViewPolicyclaimsComponent,
      },
      {
        path: 'addInsuranceType',
        component: AddInsurenceTypeComponent,
      },
      {
        path: 'viewInsuranceType',
        component: ViewInsurenceTypeComponent,
      },
      {
        path: 'addInsuranceScheme',
        component: AddInsurenceSchemeComponent,
      },
      {
        path: 'viewInsuranceScheme',
        component: ViewInsurenceSchemeComponent,
      },
      {
        path: 'addInsurancePlan',
        component: AddInsurencePlanComponent,
      },
      {
        path: 'viewInsurancePlan',
        component: ViewInsurencePlanComponent,
      },
      {
        path: 'taxPanel',
        component: TaxPanelComponent,
      },
      {
        path: 'insuranceSetting',
        component: InsuranceSettingComponent,
      },
      {
        path: 'addState',
        component: AddStateComponent,
      },
      {
        path: 'viewState',
        component: ViewStateComponent,
        children: [
          {
            path: 'updateStatus/:stateId',
            redirectTo: '/admin/updateStatus/:stateId',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'updateStatus/:stateId',
        component: UpdateStateStatusComponent,
      },
      {
        path: 'addCity',
        component: AddCityComponent,
      },
      {
        path: 'viewCity',
        component: ViewCityComponent,
      },
      {
        path: 'updateProfile',
        component: AdminProfileUpdateComponent,
      },
      {
        path: 'changePassword',
        component: AdminChangePasswordComponent,
      },
      {
        path: 'addEmployee',
        component: AddEmployeeComponent,
      },
      {
        path: 'viewEmployee',
        component: ViewEmployeeComponent,
      },
      {
        path: 'updateAgent',
        component: UpdateAgentComponentComponent,
      },
      {
        path: 'viewInsuranceDetails',
        component: CustomerInsuranceAccountComponent,
      },
      {
        path: '**',
        component: PageNotFoundComponent,
      },
    ],
  },
  {
    path: 'employee',
    component: EmployeeHomePageComponent,
    children: [
      {
        path: '',
        component: EmployeeDashBoardComponent,
      },
      {
        path: 'addAgent',
        component: AddAgentComponent,
      },
      {
        path: 'viewAgent',
        component: ViewAgentComponent,
        children: [
          {
            path: 'updateAgent',
            redirectTo: '/employee/updateAgent',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewCommission',
        component: ViewCommissionComponent,
      },
      {
        path: 'viewCommissionWithrawl',
        component: ViewCommissionWithdrawlComponent,
      },
      {
        path: 'updateAgent',
        component: UpdateAgentComponentComponent,
      },
      {
        path: 'updateProfile',
        component: AdminProfileUpdateComponent,
      },
      {
        path: 'changePassword',
        component: AdminChangePasswordComponent,
      },
      {
        path: 'viewCustomers',
        component: AdminViewCustomersComponent,
        children: [
          {
            path: 'viewDocuments',
            redirectTo: '/employee/viewDocuments',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewInsuranceAccountDetailgent',
        component: AdminViewInsuranceAccountDetailComponent,
        children: [
          {
            path: 'viewInsuranceDetails',
            redirectTo: '/employee/viewInsuranceDetails',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: 'viewInsuranceDetails',
        component: CustomerInsuranceAccountComponent,
      },
      {
        path: 'viewPayments',
        component: AdminViewPaymentsComponent,
      },
      {
        path: 'viewPolicyclaims',
        component: AdminViewPolicyclaimsComponent,
      },
    ],
  },
  {
    path: 'customer',
    component: CustomerHomePageComponent,
    children: [
      {
        path: '',
        component: CustomerDashBoardComponent,
      },
      {
        path: 'profile',
        component: CustomerProfileUpdateComponent,
      },
      {
        path: 'document',
        component: CustomerDocumetsComponent,
      },
      {
        path: 'changePassword',
        component: CustomerChangePasswordComponent,
      },
      {
        path: 'viewAccount',
        component: CustomerViewInsuranceAccountComponent,
      },
      {
        path: 'changePassword',
        component: CustomerChangePasswordComponent,
      },
      {
        path: 'viewAccount',
        component: CustomerViewInsuranceAccountComponent,
      },
      {
        path: 'query',
        component: CustomerQueryComponent,
      },
      {
        path: 'feedback',
        component: CustomerViewFeedbackComponent,
      },
      {
        path: 'customerViewPlan',
        component: CustomerViewPlanComponent,
      },
      {
        path: 'makePayment',
        component: CustomerMakePaymentComponent,
      },
    ],
  },
  {
    path: 'agent',
    component: AgentHomePageComponent,
    children: [
      {
        path: '',
        component: AgentDashBoardComponent,
      },
      {
        path: 'agentUpdate',
        component: AgentupdateComponent,
      },
      {
        path: 'changePassword',
        component: AgentchangePasswordComponent,
      },
      {
        path: 'marketing',
        component: AgentMarketingComponent,
      },
      {
        path: 'viewCustomer',
        component: AgentViewCustomersComponent,
      },
      {
        path: 'InsuranceAccount',
        component: AgentViewInsuranceAccountDetailComponent,
      },
      {
        path: 'viewPayment',
        component: AgentViewPaymentsComponent,
      },
      {
        path: 'viewPolicyClaim',
        component: AgentViewPolicyclaimsComponent,
      },
      {
        path: 'viewCommision',
        component: AgentViewCommissionsComponent,
      },
      {
        path: 'viewCommissionWithdraw',
        component: AgentViewCommissionWithdrawalsComponent,
      },
      {
        path: 'withdrawAmount',
        component: AgentWithdrawComponent,
      },
    ],
  },
  {
    path: '**',
    component: PageNotFoundComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const routingComponents = [
  AppBodyComponent,
  CustomerLoginComponent,
  RegistrationComponent,
  AgentLoginComponent,
  EmployeeLoginComponent,
  AdminLoginComponent,
  AdminNavBarComponent,
  PageNotFoundComponent,
  AdminDashboardComponent,
  AdminViewPolicyclaimsComponent,
  AdminViewPaymentsComponent,
  AdminViewInsuranceAccountDetailComponent,
  AdminViewCustomersComponent,
  AdminViewFeedBackComponent,
  AddInsurenceTypeComponent,
  ViewInsurenceTypeComponent,
  AddInsurenceSchemeComponent,
  ViewInsurenceSchemeComponent,
  AddInsurencePlanComponent,
  ViewInsurencePlanComponent,
  TaxPanelComponent,
  InsuranceSettingComponent,
  AddCityComponent,
  ViewCityComponent,
  AddStateComponent,
  ViewStateComponent,
  AdminProfileUpdateComponent,
  AdminChangePasswordComponent,
  AddEmployeeComponent,
  ViewEmployeeComponent,
  UpdateAgentComponentComponent,
  CustomerInsuranceAccountComponent,
  EmployeeHomePageComponent,
  EmployeeDashBoardComponent,
  UpdateStateStatusComponent,
];
