import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Credentials } from '../model/credential';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  value!:string;
  credentialFrom = this.formBuilder.group({
    email:new FormControl(),
    password: new FormControl(),
    
  });
  constructor(private formBuilder:FormBuilder,private service:EmployeeServiceService,private router:Router) { }
  // user!: LoginResponse;
  ngOnInit(): void {
  }
  login() {
    
    console.log(this.credentialFrom.value);
    
    this.service.auth(this.credentialFrom.value).subscribe({
      next:(response)=>{
        let loginResponse:Credentials = response;
        
        if (loginResponse.role === "ROLE_Admin") {
          localStorage.setItem("username", loginResponse.name);
          localStorage.setItem("ROLE", loginResponse.role);
          localStorage.setItem("userId", String(loginResponse.id));
          this.router.navigate(["/admin"])
        }
        
      },
      error: (err) => {
        this.value="Invalid Credentials"
        
      }
    })
}

}
