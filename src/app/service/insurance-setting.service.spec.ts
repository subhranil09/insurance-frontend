import { TestBed } from '@angular/core/testing';

import { InsuranceSettingService } from './insurance-setting.service';

describe('InsuranceSettingService', () => {
  let service: InsuranceSettingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsuranceSettingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
