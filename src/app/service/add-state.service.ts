import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { State } from '../model/state';

@Injectable({
  providedIn: 'root'
})
export class AddStateService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }
  addState(state: object) { 

    return this.http.post(this.url + `/admin/addState`, state);

  }
  getActiveStates() { 
    return this.http.get<State[]>(this.url + `/admin/getActiveStates`);
  }
  getStates() { 

    return this.http.get<State[]>(this.url + `/admin/getStates`);

  }
  getStateById(stateId:number) { 

    return this.http.get<State>(this.url + `/admin/getStatById/{stateId}`);

  }
  updateStateStatus(status:string,stateId:number) { 

    return this.http.get(this.url + `/admin/updateStatestatus/${status}/${stateId}`);

  }
}
