import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Payment } from '../model/payment';

@Injectable({
  providedIn: 'root',
})
export class InstallmentServiceService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  getPaymentsByCustomerId(customerId: string) {
    return this.http.get<Payment[]>(
      this.url + `/customers/getPaymentsByCustomerId/${customerId}`
    );
  }
  addPayment(payment: object, InsuranceAccountId: number) {
    console.log(typeof InsuranceAccountId);

    return this.http.post(
      this.url + `/customers/addPayment/${InsuranceAccountId}`, payment
    );
  }
}
