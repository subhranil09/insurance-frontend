import { Injectable } from '@angular/core';
import { InsurancePlan } from '../model/insurancePlan';

@Injectable({
  providedIn: 'root'
})
export class GetPlanDetalsService {

  constructor() { }
  public plan!: InsurancePlan;

  setPlan(plan: InsurancePlan) { 
    this.plan = plan;
  }
  getPlan() { 
    return this.plan;
  }
}
