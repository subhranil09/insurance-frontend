import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IInsuranceType } from '../model/InsuranceType';

@Injectable({
  providedIn: 'root'
})
export class InsuranceTypeServiceService {

  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }
  getInsuranceTypes() { 

    return this.http.get<IInsuranceType[]>(this.url + `/admin/getInsurenceTypeList`);

  }
  getInsuranceTypesById(insuranceTypeId:number) { 

    return this.http.get<IInsuranceType>(this.url + `/admin/getInsurenceTypeById/${insuranceTypeId}`);

  }
  getActiveInsuranceTypes() { 

    return this.http.get<IInsuranceType[]>(this.url + `/admin/getActiveInsurenceTypeList`);

  }
  updateInsuranceTypes(insuranceType:any) { 

    return this.http.put(this.url + `/admin/updateInsuranceType`,insuranceType);

  }
}
