import { TestBed } from '@angular/core/testing';

import { InsuranceAccountService } from './insurance-account.service';

describe('InsuranceAccountService', () => {
  let service: InsuranceAccountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsuranceAccountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
