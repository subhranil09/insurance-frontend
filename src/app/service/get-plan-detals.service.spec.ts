import { TestBed } from '@angular/core/testing';

import { GetPlanDetalsService } from './get-plan-detals.service';

describe('GetPlanDetalsService', () => {
  let service: GetPlanDetalsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetPlanDetalsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
