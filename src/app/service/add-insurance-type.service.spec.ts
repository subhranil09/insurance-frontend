import { TestBed } from '@angular/core/testing';

import { AddInsuranceTypeService } from './add-insurance-type.service';

describe('AddInsuranceTypeService', () => {
  let service: AddInsuranceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddInsuranceTypeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
