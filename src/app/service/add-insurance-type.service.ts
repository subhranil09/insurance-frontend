import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IInsuranceType } from '../model/InsuranceType';

@Injectable({
  providedIn: 'root'
})
export class AddInsuranceTypeService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }
  postInsuranceType(insutanceType: object) : Observable<IInsuranceType>{ 
    return this.http.post<IInsuranceType>(this.url + `/admin/addInsuranceType`, insutanceType);
  }
  putInsuranceTypeImage(file: any,insuranceId:number) { 

    let formParams = new FormData();
    formParams.append('file', file)
    return this.http.put(this.url + `/admin/addInsuranceType/${insuranceId}`,formParams);

  }
  getInsuranceTypeImage(image_src:String) { 

    return this.http.get(this.url + `/admin/addInsuranceType/${image_src}`);

  }
}
