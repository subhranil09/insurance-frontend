import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InsuranceAccount } from '../model/insuranceAccount';

@Injectable({
  providedIn: 'root',
})
export class InsuranceAccountService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  addInsuranceAccount(insuranceAccount: Object, customerId: any) {
    return this.http.post<InsuranceAccount>(
      this.url + `/customers/addInsurancenAccount/${customerId}`,
      insuranceAccount
    );
  }
  addCommission(Commission: Object, agentCode: any) {
    return this.http.post(
      this.url + `/customers/addCommission/${agentCode}`,
      Commission
    );
  }
  getInsuranceAccountByCustomerId(customerId: string) {
    return this.http.get<InsuranceAccount[]>(
      this.url + `/customers/getInsuranceAccountsByCustomerId/${customerId}`
    );
  }
  installmentCommission(Commission: Object, agentCode: any, insuranceSchemeId: number) {
    return this.http.post(
      this.url + `/customers/installmentCommission/${agentCode}/${insuranceSchemeId}`,
      Commission
    );
  }
}
