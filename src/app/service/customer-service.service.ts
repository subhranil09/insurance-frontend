import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Credentials } from '../model/credential';
import { Customer } from '../model/customer';
import { CustomerOrder } from '../model/customerOrder';

@Injectable({
  providedIn: 'root'
})
export class CustomerServiceService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  // window() : any {
  //   // return the global native browser window object
  //   return window;
  // }
  get nativeWindow(): any {
    return window;
  }
  auth(value: object) {

    return this.http.post<Credentials>(this.url + `/unauth/loginCustomer`, value);
  }

  order_request(data: any) {
    return this.http.post<CustomerOrder>(this.url + `/customers/createOrder`, data);
  }

  updatePassword(password: object, userId: string) {

    return this.http.put(this.url + `/customers/changePassword/${userId}`, password);

  }

  updateCustomerProfile(customer: object) {

    return this.http.put(this.url + `/customers/update`, customer);

  }

  getCustomerProfile(userId: string) {

    return this.http.get<Customer>(this.url + `/customers/getCustomerProfile/${userId}`);

  }

}
