import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from '../model/city';

@Injectable({
  providedIn: 'root'
})
export class AddCityService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  addCity(city: Object, stateId: number) { 
  
    return this.http.post<City>(this.url + `/admin/addCity/${stateId}`, city);
  }
  getCities() { 
    return this.http.get<City[]>(this.url + `/admin/getCities`);
  }

  updateCityStatus(status:string,cityId:number) { 

    return this.http.get(this.url + `/admin/updateCitystatus/${status}/${cityId}`);

  }

}
