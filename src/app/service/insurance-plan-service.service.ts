import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InsurancePlan } from '../model/insurancePlan';

@Injectable({
  providedIn: 'root',
})
export class InsurancePlanServiceService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) {}

  addInsurancePlan(insurancePlan: object, insuranceTypeId: number) {
    return this.http.post(
      this.url + `/admin/addInsurancePlan/${insuranceTypeId}`,
      insurancePlan
    );
  }

  getInsurancePlans() {
    return this.http.get<InsurancePlan[]>(
      this.url + `/admin/getInsurencePlanList`
    );
  }
  getActiveInsurancePlans() {
    return this.http.get<InsurancePlan[]>(
      this.url + `/customers/getInsurenceActivePlanList`
    );
  }
  updateInsurancePlan(insurancePlan: object) {
    return this.http.put(
      this.url + `/admin/updateInsurancePlan`,
      insurancePlan
    );
  }
  getInsurancePlanById(insurancePlanId: number) {
    return this.http.put(
      this.url + `/customers/getInsurencePlan/${insurancePlanId}`,
      insurancePlanId
    );
  }
}
