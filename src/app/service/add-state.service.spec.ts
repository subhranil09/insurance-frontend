import { TestBed } from '@angular/core/testing';

import { AddStateService } from './add-state.service';

describe('AddStateService', () => {
  let service: AddStateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AddStateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
