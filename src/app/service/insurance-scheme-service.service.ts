import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InsuranceScheme } from '../model/insuranceScheme';

@Injectable({
  providedIn: 'root'
})
export class InsuranceSchemeServiceService {

  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  addInsurancecheme(insuranceSchme: object,insuranceTypeId:number) { 
    return this.http.post(this.url + `/admin/addInsuranceScheme/${insuranceTypeId}`, insuranceSchme);
  }


  getInsuranceSchemes() { 

    return this.http.get<InsuranceScheme[]>(this.url + `/admin/getInsurenceSchemeList`);

  }
  getActiveInsuranceSchemes() { 

    return this.http.get<InsuranceScheme[]>(this.url + `/admin/getActiveInsurenceSchemeList`);

  }
  updateInsuranceScheme(insuranceSchme:object) { 

    return this.http.put(this.url + `/admin/updateInsuranceScheme`,insuranceSchme);

  }
  getActiveInsuranceSchemeByTypeId(insuranceTypeId: number) { 
    return this.http.get<InsuranceScheme[]>(this.url + `/admin/getActiveInsuranceSchemesByTypeId/${insuranceTypeId}`);
  }
}
