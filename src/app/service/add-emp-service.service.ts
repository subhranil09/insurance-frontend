import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Employee } from '../model/employee';

@Injectable({
  providedIn: 'root'
})
export class AddEmpServiceService {

  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  addEmployee(employee: object) { 

    return this.http.post(this.url + `/admin/createemployee`, employee);

  }
  getEmployees() { 

    return this.http.get<Employee[]>(this.url + `/admin/getEmployees`);

  }
  deleteEmployees(employeeId:number) { 

    return this.http.get(this.url + `/admin/removeEmployee/${employeeId}`);

  }
}
