import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InsuranceSettings } from '../model/insuranceSettings';

@Injectable({
  providedIn: 'root'
})
export class InsuranceSettingService {

  url = 'http://localhost:8080'; 

  constructor(private http: HttpClient) { } 
   
  updateTax(tax:object) {  
    console.log
    return this.http.put(this.url + `/admin/updateTax`,tax); 
 
  } 
  getSettings() {  
 
    return this.http.get<InsuranceSettings>(this.url +`/admin/getInsuranceSetting`); 
 
  } 
  updateSettings(setting:object) {  
    return this.http.put(this.url + `/admin/updateInsuranceSetting`,setting); 
 
  }
}
