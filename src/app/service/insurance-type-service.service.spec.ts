import { TestBed } from '@angular/core/testing';

import { InsuranceTypeServiceService } from './insurance-type-service.service';

describe('InsuranceTypeServiceService', () => {
  let service: InsuranceTypeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsuranceTypeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
