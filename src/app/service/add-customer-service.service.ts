import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../model/customer';
@Injectable({
  providedIn: 'root',
})
export class AddCustomerServiceService {
  constructor(private http: HttpClient) {}

  url = 'http://localhost:8080';
  postCustomer(customer: object) {
    return this.http.post(this.url + `/unauth/register`, customer);
  }
  getCustomers() {
    return this.http.get<Customer[]>(this.url + `/agent/getCustomers`);
  }
}
