import { TestBed } from '@angular/core/testing';

import { InstallmentServiceService } from './installment-service.service';

describe('InstallmentServiceService', () => {
  let service: InstallmentServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InstallmentServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
