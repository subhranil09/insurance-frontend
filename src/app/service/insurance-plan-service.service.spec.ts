import { TestBed } from '@angular/core/testing';

import { InsurancePlanServiceService } from './insurance-plan-service.service';

describe('InsurancePlanServiceService', () => {
  let service: InsurancePlanServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsurancePlanServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
