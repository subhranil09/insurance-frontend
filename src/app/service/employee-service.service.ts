import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Commission } from '../model/commission';
import { CommissionWithdrawl } from '../model/commissionWithdrawl';
import { Credentials } from '../model/credential';
import { InsuranceAccount } from '../model/insuranceAccount';

@Injectable({
  providedIn: 'root',
})
export class EmployeeServiceService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  auth(value: object) {
    console.log(value);

    return this.http.post<Credentials>(
      this.url + `/unauth/loginEmployee`,
      value
    );
  }
  getInsuranceAccount() {
    return this.http.get<InsuranceAccount[]>(
      this.url + `/agent/getInsuranceAccounts`
    );
  }

  updatePassword(password: object, userId: string) {

    return this.http.put(this.url + `/admin/changePassword/${userId}`, password);

  }
  getCommissions() {
    return this.http.get<Commission[]>(
      this.url + `/employee/getCommissions`
    );
  }
  getCommissionWithdrawals() {
    return this.http.get<CommissionWithdrawl[]>(
      this.url + `/employee/getCommissionWithdrawals`
    );
  }
}
