import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Agent } from '../model/agent';
import { Commission } from '../model/commission';
import { CommissionWithdrawl } from '../model/commissionWithdrawl';
import { Credentials } from '../model/credential';
import { InsuranceAccount } from '../model/insuranceAccount';

@Injectable({
  providedIn: 'root',
})
export class AgentService {
  url = 'http://localhost:8080';
  constructor(private http: HttpClient) { }

  addAgent(agent: object) {
    return this.http.post(this.url + `/employee/addAgent`, agent);
  }
  getAgents() {
    return this.http.get<Agent[]>(this.url + `/employee/getAgents`);
  }
  auth(value: object) {
    console.log(value);

    return this.http.post<Credentials>(this.url + `/unauth/loginAgent`, value);
  }
  getInsuranceAccount() {
    return this.http.get<InsuranceAccount[]>(
      this.url + `/agent/getInsuranceAccounts`
    );
  }
  getCommissionsByAgentId(agentId: string) {
    return this.http.get<Commission[]>(
      this.url + `/agent/getCommissionsByAgentId/${agentId}`
    );
  }
  getCommissionWithdrawalsByAgentId(agentId: string) {
    return this.http.get<CommissionWithdrawl[]>(
      this.url + `/agent/getCommissionWithdrawalsByAgentId/${agentId}`
    );
  }
  addCommissionWithdrawal(
    commissionWithdrawReq: Object,
    commissionAccountId: number
  ) {
    return this.http.post(
      this.url + `/agent/addCommissionWithdrawal/${commissionAccountId}`,
      commissionWithdrawReq
    );
  }
  updatePassword(password: object, userId: string) {

    return this.http.post(this.url + `/agent/changePassword/${userId}`, password);

  }
  updateAgentProfile(agent: object) {

    return this.http.put(this.url + `/agent/update`, agent);

  }

  getAgentProfile(userId: string) {

    return this.http.get<Agent>(this.url + `/agent/getAgentByByAgentId/${userId}`);

  }
}
