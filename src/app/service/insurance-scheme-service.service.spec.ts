import { TestBed } from '@angular/core/testing';

import { InsuranceSchemeServiceService } from './insurance-scheme-service.service';

describe('InsuranceSchemeServiceService', () => {
  let service: InsuranceSchemeServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InsuranceSchemeServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
