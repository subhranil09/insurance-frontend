import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Credentials } from '../model/credential';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agent-login',
  templateUrl: './agent-login.component.html',
  styleUrls: ['./agent-login.component.css'],
})
export class AgentLoginComponent implements OnInit {
  value!: string;
  credentialFrom = this.formBuilder.group({
    email: new FormControl(),
    password: new FormControl(),
  });
  constructor(
    private formBuilder: FormBuilder,
    private service: AgentService,
    private router: Router
  ) {}
  // user!: LoginResponse;
  ngOnInit(): void {}
  login() {
    console.log(this.credentialFrom.value);

    this.service.auth(this.credentialFrom.value).subscribe({
      next: (response) => {
        let loginResponse: Credentials = response;
        localStorage.setItem('username', loginResponse.name);
        localStorage.setItem('ROLE', loginResponse.role);
        localStorage.setItem('userId', String(loginResponse.id));
        console.log(response);
        this.router.navigate(['/agent']);
      },
      error: (err) => {
        this.value = 'Invalid Credentials';
      },
    });
  }
}
