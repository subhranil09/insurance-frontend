import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { InsurancePlan } from '../model/insurancePlan';
import { AddInsuranceTypeService } from '../service/add-insurance-type.service';
import { GetPlanDetalsService } from '../service/get-plan-detals.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { CustomerServiceService } from '../service/customer-service.service';
import { Customer } from '../model/customer';
import { CustomerOrder } from '../model/customerOrder';
import Swal from 'sweetalert2';
import { InsuranceAccount } from '../model/insuranceAccount';
import { CreateInsuranceAccount } from '../model/createInsuranceAccount';
import { InsuranceAccountService } from '../service/insurance-account.service';
import { Commissionadd } from '../model/commissionadd';

@Component({
  selector: 'app-customer-view-plan',
  templateUrl: './customer-view-plan.component.html',
  styleUrls: ['./customer-view-plan.component.css'],
})
export class CustomerViewPlanComponent implements OnInit {
  file!: any;
  constructor(
    private getPlanService: GetPlanDetalsService,
    private typeService: AddInsuranceTypeService,
    private customerService: CustomerServiceService,
    private modalService: NgbModal,
    private insuranceAccountService: InsuranceAccountService
  ) { }
  plan!: InsurancePlan;
  years!: number;
  totalInvestmentAmount!: number;
  months!: number;
  installmentAmount!: number;
  interestAmount!: number;
  totalAmount!: number;
  closeResult: string = '';
  datecreated = new Date();
  maturityDate!: Date;
  insuranceAccount: CreateInsuranceAccount = new CreateInsuranceAccount();
  commission: Commissionadd = new Commissionadd();
  customerId!: any;
  agentCode!: string;
  NewinsuranceAccount!: InsuranceAccount;
  ngOnInit(): void {
    this.customerId = localStorage.getItem('userId');
    this.plan = this.getPlanService.getPlan();
    console.log(this.plan);
    // this.typeService.getInsuranceTypeImage(this.plan.insuranceScheme.insuranceType.imageSrc).subscribe(
    //   {
    //     next: (response) => {
    //       this.file = response;
    //       console.log(this.file);
    //     },
    //     error: (err) => {
    //       alert("error")
    //     }

    //   }
    // )
  }

  calculate() {
    if (
      this.years < this.plan.minumumPolicyTerm ||
      this.years > this.plan.maximumInvestmentAmount
    ) {
      alert('please select correct policy term');
    } else {
      if (
        this.totalInvestmentAmount < this.plan.minimumInvestmentAmount ||
        this.totalInvestmentAmount > this.plan.maximumInvestmentAmount
      ) {
        alert('InvestMent ammount not in renge');
      } else {
        this.interestAmount = parseFloat(
          (
            this.totalInvestmentAmount *
            (this.plan.profitRatio / 100) *
            this.years
          ).toFixed(2)
        );
        // console.log(Math.round(this.interestAmount * 100) / 100);

        this.installmentAmount = parseFloat(
          (this.totalInvestmentAmount / this.months).toFixed(2)
        );
        console.log(this.installmentAmount);

        this.totalAmount = this.totalInvestmentAmount + this.interestAmount;
        this.maturityDate = new Date();
        console.log(this.datecreated);
        this.maturityDate.setFullYear(
          this.datecreated.getFullYear() + this.years
        );
        console.log(this.maturityDate.getTime());
      }
    }
  }
  order!: CustomerOrder;
  purchace() {
    let amount = String(this.installmentAmount);
    let data = {
      amount: amount,
      info: 'order_request',
      username: localStorage.getItem('username'),
    };
    var newThis = this;
    this.customerService.order_request(data).subscribe({
      next: (response) => {
        this.order = response;
        if (!!this.order) {
          let options = {
            Key: 'rzp_test_uz0TSGP9WGLAXg',
            amount: amount,
            currency: 'INR',
            name: this.plan.insurancePlan,
            description: 'payment',
            order_id: response.id,
            image: '',
            handler: function (response: any) {
              let paymentId = response.razorpay_payment_id;
              console.log(response.razorpay_payment_id);
              console.log(response.razorpay_order_id);
              console.log(response.razorpay_signature);

              newThis.insuranceAccount.insurancePlanId =
                newThis.plan.insurancePlanId;
              newThis.insuranceAccount.insuranceSchemeId =
                newThis.plan.insuranceScheme.insuranceSchemeId;

              newThis.insuranceAccount.insurancePlan =
                newThis.plan.insurancePlan;
              newThis.insuranceAccount.insuranceScheme =
                newThis.plan.insuranceScheme.insuranceScheme;

              newThis.insuranceAccount.dateCreated =
                newThis.datecreated.getTime();
              newThis.insuranceAccount.maturityDate =
                newThis.maturityDate.getTime();

              newThis.insuranceAccount.policyTerm = newThis.months;
              newThis.insuranceAccount.totalPremiumAmount =
                newThis.totalInvestmentAmount;
              newThis.insuranceAccount.sumAssured = newThis.totalAmount;
              newThis.insuranceAccount.agentCode = newThis.agentCode;

              newThis.insuranceAccountService
                .addInsuranceAccount(
                  newThis.insuranceAccount,
                  newThis.customerId
                )
                .subscribe({
                  next: (response) => {
                    newThis.NewinsuranceAccount = response;
                    newThis.commission.insuranceAccountNumber =
                      newThis.NewinsuranceAccount.insuranceAccountId;
                    newThis.commission.commissionDate = newThis.datecreated.getTime();
                    newThis.commission.commissionPercentage =
                      newThis.plan.insuranceScheme.commissionForNewRegistration;

                    newThis.commission.commissionAmount = newThis.totalAmount;

                    console.log(newThis.agentCode);
                    if (newThis.agentCode != null) {
                      newThis.insuranceAccountService
                        .addCommission(newThis.commission, newThis.agentCode)
                        .subscribe();
                    }
                  },
                });
              console.log(newThis.NewinsuranceAccount.insuranceAccountId);

              // newThis.commission.insuranceAccountNumber =
              //   newThis.NewinsuranceAccount.insuranceAccountId;
              // newThis.commission.commissionDate = newThis.datecreated.getTime();
              // newThis.commission.commissionPercentage =
              //   newThis.plan.insuranceScheme.commissionForNewRegistration;

              // newThis.commission.commissionAmount = newThis.totalAmount;

              // console.log(newThis.agentCode);
              // if (newThis.agentCode != null) {
              //   newThis.insuranceAccountService
              //     .addCommission(newThis.commission, newThis.agentCode)
              //     .subscribe();
              // }
              Swal.fire('Payment Successful');
            },
            prefill: {
              name: '',
              email: '',
              contact: '',
            },
            theme: {
              color: '#3399cc',
            },
          };
          let rzp = new this.customerService.nativeWindow.Razorpay(options);
          rzp.on('payment.failed', function (response: any) {
            console.log(response.error.code);
            console.log(response.error.description);
            console.log(response.error.source);
            console.log(response.error.step);
            console.log(response.error.reason);
            console.log(response.error.metadata.order_id);
            console.log(response.error.metadata.payment_id);

            alert('Oops Payment Failed!!!');
          });
          rzp.open();
        }
      },
      error: (err) => {
        alert('Faild');
      },
    });
  }
  //bootstrap model open
  open(content: any) {
    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}
