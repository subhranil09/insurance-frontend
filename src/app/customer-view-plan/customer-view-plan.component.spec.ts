import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerViewPlanComponent } from './customer-view-plan.component';

describe('CustomerViewPlanComponent', () => {
  let component: CustomerViewPlanComponent;
  let fixture: ComponentFixture<CustomerViewPlanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerViewPlanComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerViewPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
