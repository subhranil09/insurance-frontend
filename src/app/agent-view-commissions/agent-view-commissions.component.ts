import { Component, OnInit } from '@angular/core';
import { Commission } from '../model/commission';
import { CommissionWithdrawlReq } from '../model/commissionWithDrawlRequest';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agent-view-commissions',
  templateUrl: './agent-view-commissions.component.html',
  styleUrls: ['./agent-view-commissions.component.css'],
})
export class AgentViewCommissionsComponent implements OnInit {
  constructor(private service: AgentService) {}
  commissions!: Commission[];
  agentId!: any;
  datecreated = new Date();
  commissionWithdrawReq: CommissionWithdrawlReq = new CommissionWithdrawlReq();

  ngOnInit(): void {
    this.agentId = localStorage.getItem('userId');
    console.log(this.agentId);

    this.service.getCommissionsByAgentId(this.agentId).subscribe({
      next: (response) => {
        this.commissions = response;
      },
    });
  }
  withdraw() {
    let accountNo = this.commissions[0].commissionAccount.commissionAccountId;
    this.commissionWithdrawReq.agentId = this.agentId;
    this.commissionWithdrawReq.withdrawlDate = this.datecreated.getTime();
    this.commissionWithdrawReq.withdrawnAmount =
      this.commissions[0].commissionAccount.balance;
    console.log(typeof accountNo, accountNo);

    this.service
      .addCommissionWithdrawal(this.commissionWithdrawReq, accountNo)
      .subscribe({
        next: (response) => {
          alert('Commission Withdrawl successfully Completed');
        },
        error: (err) => {
          alert('Faild');
        },
      });
  }
}
