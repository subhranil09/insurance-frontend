import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentViewCommissionsComponent } from './agent-view-commissions.component';

describe('AgentViewCommissionsComponent', () => {
  let component: AgentViewCommissionsComponent;
  let fixture: ComponentFixture<AgentViewCommissionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentViewCommissionsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentViewCommissionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
