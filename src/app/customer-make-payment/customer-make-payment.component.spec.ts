import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerMakePaymentComponent } from './customer-make-payment.component';

describe('CustomerMakePaymentComponent', () => {
  let component: CustomerMakePaymentComponent;
  let fixture: ComponentFixture<CustomerMakePaymentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerMakePaymentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerMakePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
