import { Component, OnInit } from '@angular/core';
import { Commissionadd } from '../model/commissionadd';
import { CustomerOrder } from '../model/customerOrder';
import { InsuranceAccount } from '../model/insuranceAccount';
import { InsurancePlan } from '../model/insurancePlan';
import { Payment } from '../model/payment';
import { CustomerServiceService } from '../service/customer-service.service';
import { InstallmentServiceService } from '../service/installment-service.service';
import { InsuranceAccountService } from '../service/insurance-account.service';
import { InsurancePlanServiceService } from '../service/insurance-plan-service.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-customer-make-payment',
  templateUrl: './customer-make-payment.component.html',
  styleUrls: ['./customer-make-payment.component.css'],
})
export class CustomerMakePaymentComponent implements OnInit {
  insuranceAccounts!: InsuranceAccount[];
  insurancePlans!: InsurancePlan[];

  constructor(
    private getInsuranceAccountByCustomerId: InsuranceAccountService,
    private insurancePlanService: InsurancePlanServiceService,
    private installmentService: InstallmentServiceService,
    private customerService: CustomerServiceService,
    private insuranceAccountService: InsuranceAccountService
  ) { }
  customerId!: any;
  payments!: Payment[];
  commission: Commissionadd = new Commissionadd();
  ngOnInit(): void {
    this.customerId = localStorage.getItem('userId');

    this.installmentService.getPaymentsByCustomerId(this.customerId).subscribe({
      next: (response) => {
        this.payments = response;
      },
    });
  }
  datecreated = new Date();
  order!: CustomerOrder;
  makePayment(payment: Payment) {
    payment.paymentStatus = "paid";
    let amount = Math.round(payment.installmentAmount * 100) / 100
    // let amount = String(payment.installmentAmount);
    let data = {
      amount: amount,
      info: 'order_request',
      username: localStorage.getItem('username'),
    };
    var newThis = this;
    this.customerService.order_request(data).subscribe({
      next: (response) => {
        this.order = response;
        if (!!this.order) {
          let options = {
            Key: 'rzp_test_uz0TSGP9WGLAXg',
            amount: amount,
            currency: 'INR',
            name: "",
            description: 'payment',
            order_id: response.id,
            image: '',
            handler: function (response: any) {
              let paymentId = response.razorpay_payment_id;
              console.log(response.razorpay_payment_id);
              console.log(response.razorpay_order_id);
              console.log(response.razorpay_signature);

              newThis.installmentService
                .addPayment(payment, payment.insuranceAccount.insuranceAccountId)
                .subscribe();


              newThis.commission.insuranceAccountNumber =
                payment.insuranceAccount.insuranceAccountId;
              newThis.commission.commissionDate = newThis.datecreated.getTime();
              // newThis.commission.commissionPercentage =
              //   newThis.plan.insuranceScheme.commissionForNewRegistration;

              newThis.commission.commissionAmount = payment.installmentAmount;

              if (payment.insuranceAccount.agentCode != null) {
                newThis.insuranceAccountService
                  .installmentCommission(newThis.commission, payment.insuranceAccount.agentCode, payment.insuranceAccount.insuranceSchemeId)
                  .subscribe();
              }
              Swal.fire('Payment Successful');
            },
            prefill: {
              name: '',
              email: '',
              contact: '',
            },
            theme: {
              color: '#3399cc',
            },
          };
          let rzp = new this.customerService.nativeWindow.Razorpay(options);
          rzp.on('payment.failed', function (response: any) {
            console.log(response.error.code);
            console.log(response.error.description);
            console.log(response.error.source);
            console.log(response.error.step);
            console.log(response.error.reason);
            console.log(response.error.metadata.order_id);
            console.log(response.error.metadata.payment_id);

            alert('Oops Payment Failed!!!');
          });
          rzp.open();
        }
      },
      error: (err) => {
        alert('Faild');
      },
    });


  }
}
