import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InsurancePlan } from '../model/insurancePlan';
import { GetPlanDetalsService } from '../service/get-plan-detals.service';
import { InsurancePlanServiceService } from '../service/insurance-plan-service.service';

@Component({
  selector: 'app-customer-nav-bar',
  templateUrl: './customer-nav-bar.component.html',
  styleUrls: ['./customer-nav-bar.component.css'],
})
export class CustomerNavBarComponent implements OnInit {
  insurancePlans!: InsurancePlan[];
  constructor(
    private insurancePlanService: InsurancePlanServiceService,
    private route: ActivatedRoute,
    private router: Router,
    private getPlanService: GetPlanDetalsService
  ) {}

  ngOnInit(): void {
    this.insurancePlanService.getActiveInsurancePlans().subscribe({
      next: (response) => {
        this.insurancePlans = response;
      },
    });
  }
  viewPLan(plan: InsurancePlan) {
    this.getPlanService.setPlan(plan);
    // window.location.reload();
    // this.router
    //   .navigateByUrl('/customer', { skipLocationChange: true })
    //   .then(() => {
    //     this.router.navigate(['/customer/customerViewPlan']);
    //   });
    this.router.navigate(['customer/customerViewPlan']);
  }
  logout() {
    localStorage.removeItem('username');
    localStorage.removeItem('userId');
    localStorage.removeItem('role');
    this.router.navigate(['']);
  }
}
