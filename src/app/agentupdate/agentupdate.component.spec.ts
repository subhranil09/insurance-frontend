import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentupdateComponent } from './agentupdate.component';

describe('AgentupdateComponent', () => {
  let component: AgentupdateComponent;
  let fixture: ComponentFixture<AgentupdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentupdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
