import { Component, OnInit } from '@angular/core';
import { Agent } from '../model/agent';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agentupdate',
  templateUrl: './agentupdate.component.html',
  styleUrls: ['./agentupdate.component.css']
})
export class AgentupdateComponent implements OnInit {

  userId!: any;
  agent!: Agent;
  constructor(private service: AgentService) { }

  ngOnInit(): void {
    this.userId = localStorage.getItem("userId")

    this.service.getAgentProfile(this.userId).subscribe({
      next: (response) => {
        this.agent = <Agent>response;
      }, error: e => {

      }
    })
  }
  updateAgent() {
    this.service.updateAgentProfile(this.agent).subscribe({
      next: (response) => {
        alert("Update SuccessFull")

      }, error: e => {
        alert("Update Failed")
      }
    })
  }

}
