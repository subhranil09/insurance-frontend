import { Component, OnInit } from '@angular/core';
import { CommissionWithdrawl } from '../model/commissionWithdrawl';
import { AgentService } from '../service/agent.service';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-view-commission-withdrawl',
  templateUrl: './view-commission-withdrawl.component.html',
  styleUrls: ['./view-commission-withdrawl.component.css']
})
export class ViewCommissionWithdrawlComponent implements OnInit {

  constructor(private service: EmployeeServiceService) { }
  commissionWithdrawls!: CommissionWithdrawl[];
  agentId!: any;
  totalCommissionWithdraw: number = 0;
  ngOnInit(): void {
    this.service.getCommissionWithdrawals().subscribe({
      next: (response) => {
        this.commissionWithdrawls = response;

      },
    });
  }

}
