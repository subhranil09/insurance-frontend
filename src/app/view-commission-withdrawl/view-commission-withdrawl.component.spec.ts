import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewCommissionWithdrawlComponent } from './view-commission-withdrawl.component';

describe('ViewCommissionWithdrawlComponent', () => {
  let component: ViewCommissionWithdrawlComponent;
  let fixture: ComponentFixture<ViewCommissionWithdrawlComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewCommissionWithdrawlComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewCommissionWithdrawlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
