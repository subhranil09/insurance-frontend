import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customer';
import { AddCustomerServiceService } from '../service/add-customer-service.service';

@Component({
  selector: 'app-admin-view-customers',
  templateUrl: './admin-view-customers.component.html',
  styleUrls: ['./admin-view-customers.component.css'],
})
export class AdminViewCustomersComponent implements OnInit {
  customers!: Customer[];
  constructor(private service: AddCustomerServiceService) {}

  ngOnInit(): void {
    this.service.getCustomers().subscribe({
      next: (response) => {
        this.customers = response;
      },
    });
  }
}
