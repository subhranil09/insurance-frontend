import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewFeedBackComponent } from './admin-view-feed-back.component';

describe('AdminViewFeedBackComponent', () => {
  let component: AdminViewFeedBackComponent;
  let fixture: ComponentFixture<AdminViewFeedBackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewFeedBackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminViewFeedBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
