import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { PasswordRequest } from '../model/passwordRequest';
import { CustomerServiceService } from '../service/customer-service.service';

@Component({
  selector: 'app-customer-change-password',
  templateUrl: './customer-change-password.component.html',
  styleUrls: ['./customer-change-password.component.css']
})
export class CustomerChangePasswordComponent implements OnInit {

  updatePasswordForm = this.formBuilder.group({
    oldPassword: new FormControl('',
      [Validators.required,
      ]),
    newPassword: new FormControl('',
      [Validators.required,
      ]),
    confirmPassword: new FormControl('',
      [Validators.required,
      ])
  });

  constructor(private formBuilder: FormBuilder, private passwordService: CustomerServiceService) { }
  passwordRequest: PasswordRequest = new PasswordRequest();
  ngOnInit(): void {
  }
  userId!: any;
  updatePassword() {
    if (this.updatePasswordForm.value.confirmPassword !== this.updatePasswordForm.value.newPassword) {
      alert("Confirm password dosn't match please check again");
    }
    else {
      this.userId = localStorage.getItem("userId");

      this.passwordService.updatePassword(this.updatePasswordForm.value, this.userId).subscribe(
        {
          next: (response) => {
            alert("Password is updated.");
          },
          error: (err) => {
            alert("Cannot update Password.");
          }
        }
      )
    }

  }

}
