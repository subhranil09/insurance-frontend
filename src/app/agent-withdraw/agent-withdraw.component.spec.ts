import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentWithdrawComponent } from './agent-withdraw.component';

describe('AgentWithdrawComponent', () => {
  let component: AgentWithdrawComponent;
  let fixture: ComponentFixture<AgentWithdrawComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentWithdrawComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentWithdrawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
