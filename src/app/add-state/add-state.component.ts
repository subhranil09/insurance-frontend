import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { AddStateService } from '../service/add-state.service';

@Component({
  selector: 'app-add-state',
  templateUrl: './add-state.component.html',
  styleUrls: ['./add-state.component.css']
})
export class AddStateComponent implements OnInit {
  addStateForm = this.formBuilder.group({
    state:new FormControl(),
    status: new FormControl(),
  });
  errorMsg!: any;
  constructor(private formBuilder:FormBuilder,private service:AddStateService) { }

  ngOnInit(): void {
  }
  addState() { 
    this.service.addState(this.addStateForm.value).subscribe(
      {
        next: (response) => {
          alert("Added state is Successfully ");
        },
        error: (err) => {
          alert("Cannot Add The State");
        }
      }
    )
  }

}
