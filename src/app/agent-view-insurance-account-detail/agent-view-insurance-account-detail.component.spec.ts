import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentViewInsuranceAccountDetailComponent } from './agent-view-insurance-account-detail.component';

describe('AgentViewInsuranceAccountDetailComponent', () => {
  let component: AgentViewInsuranceAccountDetailComponent;
  let fixture: ComponentFixture<AgentViewInsuranceAccountDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentViewInsuranceAccountDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentViewInsuranceAccountDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
