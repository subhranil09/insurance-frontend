import { Component, OnInit } from '@angular/core';
import { InsuranceAccount } from '../model/insuranceAccount';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agent-view-insurance-account-detail',
  templateUrl: './agent-view-insurance-account-detail.component.html',
  styleUrls: ['./agent-view-insurance-account-detail.component.css'],
})
export class AgentViewInsuranceAccountDetailComponent implements OnInit {
  constructor(private service: AgentService) {}
  insuranceAccounts!: InsuranceAccount[];
  ngOnInit(): void {
    this.service.getInsuranceAccount().subscribe({
      next: (response) => {
        this.insuranceAccounts = response;
      },
    });
  }
}
