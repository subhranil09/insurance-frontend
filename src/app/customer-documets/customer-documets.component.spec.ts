import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerDocumetsComponent } from './customer-documets.component';

describe('CustomerDocumetsComponent', () => {
  let component: CustomerDocumetsComponent;
  let fixture: ComponentFixture<CustomerDocumetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerDocumetsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerDocumetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
