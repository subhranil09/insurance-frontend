import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerViewInsuranceAccountComponent } from './customer-view-insurance-account.component';

describe('CustomerViewInsuranceAccountComponent', () => {
  let component: CustomerViewInsuranceAccountComponent;
  let fixture: ComponentFixture<CustomerViewInsuranceAccountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerViewInsuranceAccountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerViewInsuranceAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
