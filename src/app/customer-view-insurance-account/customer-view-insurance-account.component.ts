import { Component, OnInit } from '@angular/core';
import { InsuranceAccount } from '../model/insuranceAccount';
import { InsurancePlan } from '../model/insurancePlan';
import { InstallmentServiceService } from '../service/installment-service.service';
import { InsuranceAccountService } from '../service/insurance-account.service';
import { InsurancePlanServiceService } from '../service/insurance-plan-service.service';

@Component({
  selector: 'app-customer-view-insurance-account',
  templateUrl: './customer-view-insurance-account.component.html',
  styleUrls: ['./customer-view-insurance-account.component.css'],
})
export class CustomerViewInsuranceAccountComponent implements OnInit {
  insuranceAccounts!: InsuranceAccount[];
  insurancePlans!: InsurancePlan[];
  constructor(
    private getInsuranceAccountByCustomerId: InsuranceAccountService,
    private insurancePlanService: InsurancePlanServiceService,
    private installmentPayment: InstallmentServiceService
  ) {}
  customerId!: any;
  ngOnInit(): void {
    this.customerId = localStorage.getItem('userId');
    this.getInsuranceAccountByCustomerId
      .getInsuranceAccountByCustomerId(this.customerId)
      .subscribe({
        next: (response) => {
          this.insuranceAccounts = response;
        },
      });
  }
}
