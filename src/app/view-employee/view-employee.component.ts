import { Component, OnInit } from '@angular/core';
import { Employee } from '../model/employee';
import { AddEmpServiceService } from '../service/add-emp-service.service';

@Component({
  selector: 'app-view-employee',
  templateUrl: './view-employee.component.html',
  styleUrls: ['./view-employee.component.css']
})
export class ViewEmployeeComponent implements OnInit {

  employees!: Employee[];
  constructor(private service:AddEmpServiceService) { }

  ngOnInit(): void {
    this.service.getEmployees().subscribe(
      {
        next: (response) => {
          this.employees = response;
        }
      }
    )
  }
  deleteEmployee(id: number) { 
    this.service.deleteEmployees(id).subscribe(
      {
        next: (response) => {
          alert("employee deleted!!");
          window.location.reload();
        },
        error: (err) => {
          alert("employee deletinon Faild"); 
        }
      }
    )
  }
}
