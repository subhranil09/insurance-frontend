import { Component, OnInit } from '@angular/core';
import { State } from '../model/state';
import { AddStateService } from '../service/add-state.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-view-state',
  templateUrl: './view-state.component.html',
  styleUrls: ['./view-state.component.css']
})
export class ViewStateComponent implements OnInit {
  title = 'appBootstrap';
  state!: State;
  closeResult: string = '';
  constructor(private stateService:AddStateService,private modalService: NgbModal ) { }
  states!: State[];
  ngOnInit(): void {
    this.stateService.getStates().subscribe(
      {
        next: (response) => {
          this.states = response;
        }
      }
    )
  }
  updateStateStatus(status:string){ 
    this.stateService.updateStateStatus(status, this.state.stateId).subscribe(
      {
        next: (response) => {
          alert("Status update Seccessfully ");
          window.location.reload();
        },
        error: (err) => {
          alert("Cannot Add The State");
        }
      }
    )
  }


  //bootstrap model open
  open(content: any, state: any) {
    this.state = state;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
