import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddEmpServiceService } from '../service/add-emp-service.service';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  addEmployeeForm = this.formBuilder.group({
    empoyeeName: new FormControl('',
      [Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z]+$')
      ]
    ),
    emailId: new FormControl('',
      [Validators.required,
      Validators.email
      ]
    ),
    password: new FormControl('',
      [
        Validators.required,
        Validators.minLength(8)
      ]),
    status: new FormControl(),
    role: new FormControl(),
  });
  constructor(private formBuilder: FormBuilder, private service: AddEmpServiceService) { }

  ngOnInit(): void {
  }
  addEmployee(cpassword: String) {
    console.log(cpassword);
    console.log(this.addEmployeeForm.value);
    if (cpassword !== this.addEmployeeForm.value.password) {
      alert("Confirm password dosn't match please check again");

    }
    else {
      this.service.addEmployee(this.addEmployeeForm.value).subscribe(
        {
          next: (response) => {
            alert("Adding of Employee is Successfully done.");
          },
          error: (err) => {
            alert("Registration UnSuccessFull !!");
          }
        }
      )
    }
  }
}
