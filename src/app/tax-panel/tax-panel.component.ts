import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { InsuranceSettings } from '../model/insuranceSettings';
import { InsuranceSettingService } from '../service/insurance-setting.service';

@Component({
  selector: 'app-tax-panel',
  templateUrl: './tax-panel.component.html',
  styleUrls: ['./tax-panel.component.css']
})
export class TaxPanelComponent implements OnInit {
  insuranceSettings! : InsuranceSettings; 
  updateTaxForm=this.formBuilder.group({ 
    taxPercentage: new FormControl(), 
  });
  constructor(private formBuilder:FormBuilder,private settingService:InsuranceSettingService) { } 
 
  ngOnInit(): void { 
    this.settingService.getSettings().subscribe( 
      { 
        next: (response) => { 
          this.insuranceSettings = response; 
          this.updateTaxForm=this.formBuilder.group({ 
            taxPercentage: new FormControl(this.insuranceSettings.taxPercentage), 
          });
        } 
      } 
    ) 
  } 
 
  
  updateTax(){ 
    this.settingService.updateTax(this.updateTaxForm.value).subscribe( 
      { 
        next: (response) => { 
          alert("Tax Percentage is updated."); 
        }, 
        error: (err) => { 
          alert("Cannot update Tax Percentage."); 
        } 
      } 
    ) 
 
  }

}
