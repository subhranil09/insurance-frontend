import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customer';
import { AddCustomerServiceService } from '../service/add-customer-service.service';

@Component({
  selector: 'app-agent-view-customers',
  templateUrl: './agent-view-customers.component.html',
  styleUrls: ['./agent-view-customers.component.css'],
})
export class AgentViewCustomersComponent implements OnInit {
  customers!: Customer[];
  constructor(private service: AddCustomerServiceService) {}

  ngOnInit(): void {
    this.service.getCustomers().subscribe({
      next: (response) => {
        this.customers = response;
      },
    });
  }
}
