import { Component, OnInit } from '@angular/core';
import { Agent } from '../model/agent';
import { AgentService } from '../service/agent.service';
import { CustomerServiceService } from '../service/customer-service.service';

@Component({
  selector: 'app-admin-profile-update',
  templateUrl: './admin-profile-update.component.html',
  styleUrls: ['./admin-profile-update.component.css']
})
export class AdminProfileUpdateComponent implements OnInit {

  userId!: any;
  agent!: Agent;
  constructor(private service: AgentService) { }

  ngOnInit(): void {
    this.userId = localStorage.getItem("userId")

    this.service.getAgentProfile(this.userId).subscribe({
      next: (response) => {
        this.agent = <Agent>response;
      }, error: e => {

      }
    })
  }

  updateAgent() {
    this.service.updateAgentProfile(this.agent).subscribe({
      next: (response) => {
        alert("Update SuccessFull")

      }, error: e => {
        alert("Update Failed")
      }
    })
  }

}
