import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentViewCommissionWithdrawalsComponent } from './agent-view-commission-withdrawals.component';

describe('AgentViewCommissionWithdrawalsComponent', () => {
  let component: AgentViewCommissionWithdrawalsComponent;
  let fixture: ComponentFixture<AgentViewCommissionWithdrawalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentViewCommissionWithdrawalsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentViewCommissionWithdrawalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
