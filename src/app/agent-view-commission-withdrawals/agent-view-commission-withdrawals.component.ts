import { Component, OnInit } from '@angular/core';
import { CommissionWithdrawl } from '../model/commissionWithdrawl';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agent-view-commission-withdrawals',
  templateUrl: './agent-view-commission-withdrawals.component.html',
  styleUrls: ['./agent-view-commission-withdrawals.component.css'],
})
export class AgentViewCommissionWithdrawalsComponent implements OnInit {
  constructor(private service: AgentService) {}
  commissionWithdrawls!: CommissionWithdrawl[];
  agentId!: any;
  totalCommissionWithdraw: number = 0;
  ngOnInit(): void {
    this.agentId = localStorage.getItem('userId');
    console.log(this.agentId);

    this.service.getCommissionWithdrawalsByAgentId(this.agentId).subscribe({
      next: (response) => {
        this.commissionWithdrawls = response;
        this.commissionWithdrawls.forEach((item) => {
          this.totalCommissionWithdraw += item.withdrawnAmount;
        });
      },
    });
  }
}
