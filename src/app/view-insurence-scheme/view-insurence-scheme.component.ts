import { Component, OnInit } from '@angular/core';
import { InsuranceScheme } from '../model/insuranceScheme';
import { InsuranceSchemeServiceService } from '../service/insurance-scheme-service.service';

@Component({
  selector: 'app-view-insurence-scheme',
  templateUrl: './view-insurence-scheme.component.html',
  styleUrls: ['./view-insurence-scheme.component.css']
})
export class ViewInsurenceSchemeComponent implements OnInit {

  constructor(private insuranceSchemeService: InsuranceSchemeServiceService) { }

  insuranceSchemes!: InsuranceScheme[];
  ngOnInit(): void {
    this.insuranceSchemeService.getInsuranceSchemes().subscribe(
      {
        next: (response) => {
          this.insuranceSchemes = response;
        }
      }
    )
  }
}