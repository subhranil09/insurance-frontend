import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInsurenceSchemeComponent } from './view-insurence-scheme.component';

describe('ViewInsurenceSchemeComponent', () => {
  let component: ViewInsurenceSchemeComponent;
  let fixture: ComponentFixture<ViewInsurenceSchemeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewInsurenceSchemeComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewInsurenceSchemeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
