import { Component, OnInit } from '@angular/core';
import { Customer } from '../model/customer';
import { CustomerServiceService } from '../service/customer-service.service';

@Component({
  selector: 'app-customer-profile-update',
  templateUrl: './customer-profile-update.component.html',
  styleUrls: ['./customer-profile-update.component.css']
})
export class CustomerProfileUpdateComponent implements OnInit {

  userId!: any;
  customer!: Customer;
  constructor(private service: CustomerServiceService) { }

  ngOnInit(): void {
    this.userId = localStorage.getItem("userId")

    this.service.getCustomerProfile(this.userId).subscribe({
      next: (response) => {
        this.customer = <Customer>response;
        console.log(this.customer);

      }, error: e => {

      }
    })


  }

  updateCustomer() {
    this.service.updateCustomerProfile(this.customer).subscribe({
      next: (response) => {
        alert("Update SuccessFull")

      }, error: e => {
        alert("Update Failed")
      }
    })
  }

}
