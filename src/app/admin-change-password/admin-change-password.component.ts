import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Employee } from '../model/employee';
import { PasswordRequest } from '../model/passwordRequest';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-admin-change-password',
  templateUrl: './admin-change-password.component.html',
  styleUrls: ['./admin-change-password.component.css']
})
export class AdminChangePasswordComponent implements OnInit {

  updatePasswordForm = this.formBuilder.group({
    oldPassword: new FormControl('',
      [Validators.required,
      ]),
    newPassword: new FormControl('',
      [Validators.required,
      ]),
    confirmPassword: new FormControl('',
      [Validators.required,
      ])
  });
  constructor(private formBuilder: FormBuilder, private passwordService: EmployeeServiceService) { }
  passwordRequest: PasswordRequest = new PasswordRequest();
  ngOnInit(): void {
  }
  userId!: any;
  updatePassword() {
    this.userId = localStorage.getItem("userId");

    this.passwordService.updatePassword(this.updatePasswordForm.value, this.userId).subscribe(
      {


        next: (response) => {
          alert("Password is updated.");
        },
        error: (err) => {
          console.log(this.updatePasswordForm.value);
          console.log(this.userId);

          alert("Cannot update Password.");
        }
      }
    )

  }

}
