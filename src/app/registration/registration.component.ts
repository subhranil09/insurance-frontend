import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { AddCustomerServiceService } from '../service/add-customer-service.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  addUserForm = this.formBuilder.group({
    customerName: new FormControl('', [
      Validators.required,
      Validators.maxLength(50),
      Validators.pattern('^[a-zA-Z]+$'),
    ]),
    dateOfBirth: new FormControl(),
    emailId: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(8),
    ]),
    address: new FormControl('', [Validators.required]),
    state: new FormControl(),
    city: new FormControl(),
    pincode: new FormControl('', [
      Validators.required,
      Validators.pattern('[0-9]{6}'),
    ]),
    contactNumber: new FormControl('', [
      Validators.required,
      Validators.pattern('^((\\+91-?) |0)?[0-9]{10}$'),
    ]),
    nomineeName: new FormControl('', [Validators.required]),
    nomineeRelation: new FormControl(),
  });
  constructor(
    private formBuilder: FormBuilder,
    private service: AddCustomerServiceService
  ) {}

  ngOnInit(): void {}
  addUser(cpassword: String) {
    this.addUserForm.value.dateOfBirth = new Date(
      this.addUserForm.value.dateOfBirth
    ).getTime();
    if (cpassword !== this.addUserForm.value.password) {
      alert("Confirm password dosn't match please check again");
    } else {
      this.service.postCustomer(this.addUserForm.value).subscribe({
        next: (response) => {
          alert('Registration SuccessFull');
        },
        error: (err) => {
          alert('Registration UnuccessFull');
        },
      });
    }
  }
}
