import { State } from './state';

export class City {
  cityId!: number;
  city!: string;
  status!: string;
  state!: State;
}
