import { CommissionAccount } from './commissionAccount';

export class CommissionWithdrawl {
  commissionWithdrawalId!: number;
  withdrawnAmount!: number;
  withdrawlDate!: number;
  agentId!: number;
  commissionAccount!: CommissionAccount;
}
