import { InsuranceAccount } from './insuranceAccount';

export class Customer {
  customerId!: number;
  customerName!: string;
  password!: string;
  emailId!: string;
  contactNumber!: string;
  dateOfBirth!: number;
  state!: string;
  city!: string;
  address!: string;
  pincode!: string;
  nomineeName!: string;
  nomineeRelation!: string;
  role!: string;
}
