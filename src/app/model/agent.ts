export class Agent {  
    agentId!: number; 
    agentName!: string; 
    agentCode!: string; 
    password!:string; 
    emailId!: string; 
    agentAddress!: string; 
    qualification!:string; 
    status!: string; 
    role!: string;   
}