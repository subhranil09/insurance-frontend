export class IInsuranceType { 
    insuranceTypeId!: number;
    insuranceType!: string;
    imageSrc!: string;
    status!:string
}