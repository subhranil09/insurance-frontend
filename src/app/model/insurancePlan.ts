import { InsuranceScheme } from "./insuranceScheme";

export class InsurancePlan {
    insurancePlanId!: number;
    insurancePlan!: string;
    maximumPolicyTerm!: number;
    minumumPolicyTerm!: number;
    minimumAge!: number;
    maximumAge!: number;
    minimumInvestmentAmount!: number;
    maximumInvestmentAmount!: number;
    profitRatio!: number;
    status!: string
    insuranceScheme!: InsuranceScheme;
}