export class CreateInsuranceAccount {
  insurancePlanId!: number;
  insuranceSchemeId!: number;
  insurancePlan!: string;
  insuranceScheme!: string;
  dateCreated!: number;
  maturityDate!: number;
  policyTerm!: number;
  totalPremiumAmount!: number;
  sumAssured!: number;
  agentCode!: string;
}
