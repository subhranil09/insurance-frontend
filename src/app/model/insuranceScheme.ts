import { IInsuranceType } from "./InsuranceType";

export class InsuranceScheme { 
    insuranceSchemeId!: number;
    insuranceTypeId!: number;
    insuranceScheme!: string;
    commissionForNewRegistration!: number;
    commissionForInstallmentPayment!: number;
    note!: string;
    status!: string
    insuranceType!: IInsuranceType
}