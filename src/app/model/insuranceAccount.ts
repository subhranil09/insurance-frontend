import { Customer } from './customer';

export class InsuranceAccount {
  insuranceAccountId!: number;
  insuranceSchemeId!: number;
  insurancePlanId!: number;
  insurancePlan!: string;
  insuranceScheme!: string;
  dateCreated!: number;
  maturityDate!: number;
  policyTerm!: number;
  totalPremiumAmount!: number;
  sumAssured!: number;
  customer!: Customer;
  agentCode!: string;
  status!: string;
  balance!:number
}
