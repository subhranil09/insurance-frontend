export class CommissionAccount {
  agentCode!: string;
  agentId!: number;
  balance!: number;
  commissionAccountId!: number;
}
