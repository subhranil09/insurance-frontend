import { InsuranceAccount } from './insuranceAccount';

export class Payment {
  paymentId!: number;
  installmentNo!: number;
  installmentDate!: number;
  installmentAmount!: number;
  paymentStatus!: string;
  paidDate!: number;
  taxAmount!: number;
  insuranceAccount!: InsuranceAccount;
}
