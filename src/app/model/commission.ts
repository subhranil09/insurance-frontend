import { CommissionAccount } from './commissionAccount';

export class Commission {
  commissionId!: number;
  insuranceAccountNumber!: number;
  agentId!: number;
  commissionAmount!: number;
  commissionPercentage!: number;
  commissionDate!: number;
  commissionAccount!: CommissionAccount;
}
