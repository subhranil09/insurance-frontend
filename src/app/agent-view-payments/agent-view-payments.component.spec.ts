import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentViewPaymentsComponent } from './agent-view-payments.component';

describe('AgentViewPaymentsComponent', () => {
  let component: AgentViewPaymentsComponent;
  let fixture: ComponentFixture<AgentViewPaymentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentViewPaymentsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentViewPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
