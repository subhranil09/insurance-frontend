import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgentViewPolicyclaimsComponent } from './agent-view-policyclaims.component';

describe('AgentViewPolicyclaimsComponent', () => {
  let component: AgentViewPolicyclaimsComponent;
  let fixture: ComponentFixture<AgentViewPolicyclaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgentViewPolicyclaimsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AgentViewPolicyclaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
