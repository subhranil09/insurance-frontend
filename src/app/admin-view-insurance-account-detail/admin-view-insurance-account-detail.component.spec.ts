import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewInsuranceAccountDetailComponent } from './admin-view-insurance-account-detail.component';

describe('AdminViewInsuranceAccountDetailComponent', () => {
  let component: AdminViewInsuranceAccountDetailComponent;
  let fixture: ComponentFixture<AdminViewInsuranceAccountDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminViewInsuranceAccountDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdminViewInsuranceAccountDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
