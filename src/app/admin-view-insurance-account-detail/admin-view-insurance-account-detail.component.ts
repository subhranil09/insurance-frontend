import { Component, OnInit } from '@angular/core';
import { InsuranceAccount } from '../model/insuranceAccount';
import { AgentService } from '../service/agent.service';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-admin-view-insurance-account-detail',
  templateUrl: './admin-view-insurance-account-detail.component.html',
  styleUrls: ['./admin-view-insurance-account-detail.component.css'],
})
export class AdminViewInsuranceAccountDetailComponent implements OnInit {
  constructor(private service: EmployeeServiceService) {}
  insuranceAccounts!: InsuranceAccount[];
  ngOnInit(): void {
    this.service.getInsuranceAccount().subscribe({
      next: (response) => {
        this.insuranceAccounts = response;
      },
    });
  }
}
