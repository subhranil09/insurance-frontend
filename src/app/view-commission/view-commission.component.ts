import { Component, OnInit } from '@angular/core';
import { Commission } from '../model/commission';
import { AgentService } from '../service/agent.service';
import { EmployeeServiceService } from '../service/employee-service.service';

@Component({
  selector: 'app-view-commission',
  templateUrl: './view-commission.component.html',
  styleUrls: ['./view-commission.component.css']
})
export class ViewCommissionComponent implements OnInit {

  constructor(private service: EmployeeServiceService) { }
  commissions!: Commission[];
  datecreated = new Date();
  ngOnInit(): void {


    this.service.getCommissions().subscribe({
      next: (response) => {
        this.commissions = response;

      },
    });
  }

}
