import { Component, OnInit } from '@angular/core';
import { Agent } from '../model/agent';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-view-agent',
  templateUrl: './view-agent.component.html',
  styleUrls: ['./view-agent.component.css']
})
export class ViewAgentComponent implements OnInit {

  constructor(private service:AgentService) { }
  agents!: Agent[];
  ngOnInit(): void {
    this.service.getAgents().subscribe(
      {
        next: (response) => {
          this.agents = response;
        }
      }
    )
  }

}
