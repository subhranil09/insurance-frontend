import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { PasswordRequest } from '../model/passwordRequest';
import { AgentService } from '../service/agent.service';

@Component({
  selector: 'app-agentchange-password',
  templateUrl: './agentchange-password.component.html',
  styleUrls: ['./agentchange-password.component.css']
})
export class AgentchangePasswordComponent implements OnInit {

  updatePasswordForm = this.formBuilder.group({
    oldPassword: new FormControl('',
      [Validators.required,
      ]),
    newPassword: new FormControl('',
      [Validators.required,
      ]),
    confirmPassword: new FormControl('',
      [Validators.required,
      ])
  });

  constructor(private formBuilder: FormBuilder, private passwordService: AgentService) { }
  passwordRequest: PasswordRequest = new PasswordRequest();
  ngOnInit(): void {
  }
  userId!: any;
  updatePassword() {
    this.userId = localStorage.getItem("userId");
    if (this.updatePasswordForm.value.confirmPassword !== this.updatePasswordForm.value.newPassword) {
      alert("Confirm password dosn't match please check again");
    }
    else {
      this.passwordService.updatePassword(this.updatePasswordForm.value, this.userId).subscribe(
        {
          next: (response) => {
            alert("Password is updated.");
          },
          error: (err) => {
            alert("Cannot update Password.");
          }
        }
      )
    }

  }
}
