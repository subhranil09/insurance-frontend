import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerViewFeedbackComponent } from './customer-view-feedback.component';

describe('CustomerViewFeedbackComponent', () => {
  let component: CustomerViewFeedbackComponent;
  let fixture: ComponentFixture<CustomerViewFeedbackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CustomerViewFeedbackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CustomerViewFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
