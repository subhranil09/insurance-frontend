import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { State } from '../model/state';
import { AddCityService } from '../service/add-city.service';
import { AddStateService } from '../service/add-state.service';

@Component({
  selector: 'app-add-city',
  templateUrl: './add-city.component.html',
  styleUrls: ['./add-city.component.css']
})
export class AddCityComponent implements OnInit {

  addCityForm = this.formBuilder.group({
    id:new FormControl(),
    city: new FormControl(),
    status: new FormControl(),
  });
  activestates!: State[];
  constructor(private formBuilder:FormBuilder,private stateService:AddStateService,private cityService:AddCityService ) { }
  stateId!: number;
  ngOnInit(): void {
    this.stateService.getActiveStates().subscribe(
      {
        next: (response) => {
          this.activestates = response;
        }
      }
    )
  }

  addCity() {
    this.stateId = this.addCityForm.value.id;
    this.cityService.addCity(this.addCityForm.value,this.stateId).subscribe(
      {
        next: (response) => {
          alert("Adding City is Successfully done.");
        },
        error: (err) => {
          alert("Cannot Add The City");
        }
      }
    )
  }
}
