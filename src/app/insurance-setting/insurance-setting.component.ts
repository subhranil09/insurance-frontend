import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { InsuranceSettings } from '../model/insuranceSettings';
import { InsuranceSettingService } from '../service/insurance-setting.service';

@Component({
  selector: 'app-insurance-setting',
  templateUrl: './insurance-setting.component.html',
  styleUrls: ['./insurance-setting.component.css']
})
export class InsuranceSettingComponent implements OnInit {

  insuranceSettings! : InsuranceSettings; 
  updateSettingForm=this.formBuilder.group({ 
    claimDeductionPercentage: new FormControl(), 
    penaltyAmountPercentage: new FormControl(),
  });
  constructor(private formBuilder:FormBuilder,private settingService:InsuranceSettingService) { } 
 
  ngOnInit(): void { 
    this.settingService.getSettings().subscribe( 
      { 
        next: (response) => { 
          this.insuranceSettings = response; 
          this.updateSettingForm=this.formBuilder.group({ 
            claimDeductionPercentage: new FormControl(this.insuranceSettings.claimDeductionPercentage), 
            penaltyAmountPercentage: new FormControl(this.insuranceSettings.penaltyAmountPercentage),
          });
        } 
      } 
    ) 
  } 
 
  
  updateSetting(){ 
    this.settingService.updateSettings(this.updateSettingForm.value).subscribe( 
      { 
        next: (response) => { 
          alert("Insurance Setting is updated."); 
        }, 
        error: (err) => { 
          alert("Cannot update Tax Percentage."); 
        } 
      } 
    ) 
 
  }

}
