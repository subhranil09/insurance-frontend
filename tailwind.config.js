/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: ["./src/**/*"],
  theme: {
    extend: {},
  },
  plugins: [require("@tailwindcss/forms")],
};
